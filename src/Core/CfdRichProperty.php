<?php
namespace SchoolTwist\Cfd\Core;

class CfdRichProperty extends CfdBase
{
    /** @var bool */
    public bool $isStatic;

    /** @var bool */
    public bool $isMeta;

    /** @var string*/
    public string $name;

    //    /** @var bool */
    //    public bool $hasStaticDefault;

    #/** @var bool */
    #public bool $hasADocComment;

    #/** @var string|null*/
    #public bool $docCommentForThisProperty;


    public bool $isTypeEnforced;

    /** @var bool */
    public bool $isNullAnAllowedType;

    /** @var bool */
    public bool $isRequired;

    /* I KNOW these needs to be removed. We just live in a multi-type world. 8/20'*/
    /** @var string*/
    public string $type;

    /** @var array*/
    public array $types;

    /** @var bool */
    public bool $hasDefault;

    /** @var mixed|null */
    public $default;

    public bool $mustBeInitialized;// This really concerns me. big-merge
}
