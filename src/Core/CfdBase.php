<?php
namespace SchoolTwist\Cfd\Core;


use TypeError;
use ReflectionClass;
use ReflectionProperty;


abstract class CfdBase implements \SchoolTwist\Validations\Contracts\ArrayableDeep, \SchoolTwist\Validations\Contracts\ArrayableShallow
{
    public CONST ALLOW_UPCONVERT_TYPES = ['integer','string'];// OVERRIDE ME IF YOU DON"T WANT THESE - I'm not positive of this 8/20'
    public const FORBIDDEN_TYPES = [ ]; // mainly to avoid confusion. Actually - bool is 'true'. Boolean is just an alias: https://stackoverflow.com/questions/44009037/php-bool-vs-boolean-type-hinting
    protected const  META_PREFIXES = ['doesHave'];
    #protected const  META_PROPERTY_NAMES = ['_ArrEnumValuePossibilities', 'fieldOrder'];
    protected const  META_PROPERTY_NAMES = [
        '_ArrEnumValuePossibilities',

    ];

    public static function deriveShortNameFromCfdClassName() : string
    {
        $fqn = get_called_class();
        return static::deriveShortNameFromCfdClassName_givenFqn($fqn);
    }

    public static function deriveShortNameFromCfdClassName_givenFqn(string $fqn) : string
    {
        if (in_array($fqn, ['string','int'])) {
            return $fqn;
        }
        return str_replace('\\','',$fqn );

         $arrFqn = explode('\\',$fqn);
         $shortTableName = array_pop($arrFqn);
         return str_replace('\\','',$fqn );
         $Words_withoutHungarian = \EtStringConvert::camelCaseToEnglish($shortTableName, ['dto','Cfd','cfdb','cfe','cftbl','tbl']);
         $shortTableName = implode(explode(' ', $Words_withoutHungarian));
         return $shortTableName;
    }


    private static function StartsWith(string $prefix, string $longString): bool
    { //https://www.geeksforgeeks.org/php-startswith-and-endswith-functions/
        $len = mb_strlen($prefix);
        return (mb_substr($longString, 0, $len) === $prefix);
    }

    private static function TrimOffFront(string $prefix, string $longString): string
    {
        $len = mb_strlen($prefix);
        return mb_substr($longString, $len);
    }

    /*
     * This should be in a utility area...
    Given an array of dto items, and a key, return an array
    class DtoTab {
         public $Slug;
        public $Text
    }
    $arrTabs = [
        new DtoTab(['Slug'=>'Merge','Text'=>'Combine']),
        new DtoTab(['Slug'=>'Trash','Text'=>'Delete'),
    ];
    $selected = $_REQUEST['Tab'];// say, 'Trash',
    if (in_array($selected,Cfd_Base::arrDto_column($arrTabs,'Slug')) {
        ....
    */
    public static function arrDto_column(array $dtoThatIsAnArray, $subKey): array
    { // inspired by php's array_column

        $arr = array_map(function (CfdBase $Dto) use ($subKey) {
            if (!isset($Dto->$subKey)) {
                $class_vars = get_class_vars(get_class($Dto));
                $arrPrettyClassVarNames = [];
                foreach ($class_vars as $name => $value) {
                    $arrKnownMeta = ['exceptKeys', 'onlyKeys']; // expand later, if needed
                    if (!in_array($name, $arrKnownMeta)) {
                        $arrPrettyClassVarNames[] = $name;
                    }
                }
                $strPrettyClassVarName = implode(', ', $arrPrettyClassVarNames);
                #\EtError::AssertTrue(0, 'error', __FILE__, __LINE__, " subKey($subKey) is not a property of " . get_class($Dto) . ". Try these valid options: $strPrettyClassVarName");
                #assert(0, 'error', __FILE__, __LINE__, " subKey($subKey) is not a property of " . get_class($Dto) . ". Try these valid options: $strPrettyClassVarName");

            }
            return $Dto->$subKey;
        }, $dtoThatIsAnArray);

        return $arr;
    }


    public function getValue_notHave(string $NameOfVariable, $DefaultVarValue_ifVarIsNull)
    {
        if (is_null($this->$NameOfVariable)) {
            return $DefaultVarValue_ifVarIsNull;
        } else {
            return $this->$NameOfVariable;
        }
    }

    public function __construct(array $asrSubmission,  $doAssumeValuesAreValid = false)
    {
        #$ut = \ClsProbe::singleton()->start(__METHOD__,'');
        $asrRichProperties = static::getRichProperties();
        if ($doAssumeValuesAreValid) {
            foreach ($asrSubmission as $key=>$val) {
                // Since valid values don't imply valid types (a 'string' just isn't the same as a CfdShortString::$value)
                //  we need to maybe upconvert.
                // It is bad to program via try catch, but php sucks here...
                $typeOfSource = gettype($val);
                $typesOfDestination = $asrRichProperties['properties'][$key]['types'];
                $didAssign = false;
                foreach ($typesOfDestination as $aPossibleDestinationType) {
                    if (is_a($val,$aPossibleDestinationType, true)  || ($aPossibleDestinationType == 'mixed')) {
                        $this->$key = $val;
                        $didAssign = true;
                        break;

//                    } elseif ($typeOfSource == 'string' && method_exists($aPossibleDestinationType, 'dbStringCastBack')) {
//                        $this->$key = $aPossibleDestinationType::dbStringCastBack($val);

                    } else {
                        $typeOfSource_shortendName = static::deriveShortNameFromCfdClassName_givenFqn($typeOfSource);
                        $upconvert_method_name = "from_{$typeOfSource_shortendName}";
                        if (method_exists($aPossibleDestinationType, $upconvert_method_name)) {
                            $newValueRightTYpe = $aPossibleDestinationType::$upconvert_method_name($val, $doAssumeValuesAreValid);
                            assert(is_a($newValueRightTYpe,$aPossibleDestinationType, true));
                            $this->$key = $newValueRightTYpe;
                            $didAssign = true;
                            unset($newValueRightTYpe);
                            break;
                        }
                    }
                }
                assert($didAssign,__FILE__.__LINE__,"CFD $key was never assigned for ".get_called_class());



                //                try {
                //                    $this->$key = $val;
                //                } catch (\TypeError $e) {
                //                    if (isset($asrRichProperties[$key]) && $asrRichProperties[$key]['doesImplementDeStringable']) {
                //                        $cn = $asrRichProperties[$key]['type'];
                //                        $dtoValid = $this->$key == $$cn::__fromString($val, true);
                //                        if (!$dtoValid->isValid) {
                //                            throw ErrorFromCfd::LogicError("Default error for $key not quickly convertable from  gettype($val) in " . get_called_class() . " Tip for programmer: The incoming value is a string. You might want to try using DeStringableInterface");
                //                        }
                //                    }
                //                }
            }
            #\ClsProbe::singleton()->stop($ut);
            return;
        }


//        jj you left off noticing this is only for cfds.  You UUID is not a cfd, but your foreighId  thing is - so thats cool.
//    but this dies in prevalidat, i think.
//      I bet if we go into preValidate, we'll something.
//      I think, in general convert strings to int is usually ok.
//      Just make sure you are solving the right problem here.
//
//      Remember, this is all about the Tbl problem. EVERYTHING there is a CFD, except pk.
//      1) Convert '0' to 0 as int. <-- prove this one first
//      2) Convert 'asldkflasdfasdf' to Cfd/Value as String


        $dtoValid = static::preValidateSubmission($asrSubmission);
        if (!$dtoValid->isValid) {


            $errorPrefix = '';//get_called_class().": ";


            #\EtError::AssertTrue(0,'error',__FILE__,__LINE__,"{$dtoValid->enumReason} {$dtoValid->message}");
            //            if ($dtoValid->enumReason == 'doesHave_MissingFor_AccompanyingProperty') {
            //                throw ErrorFromCfd::doesHave_MissingFor_AccompanyingProperty($errorPrefix . $dtoValid->message);
            //
            //            } else if ($dtoValid->enumReason == 'doesHave_ControlledProperty_notSet') {
            //                throw ErrorFromCfd::doesHave_ControlledProperty_notSet($errorPrefix . $dtoValid->message);
            //
            //            } else if ($dtoValid->enumReason == 'doesHave_ControlledProperty_notSet') {
            //                throw ErrorFromCfd::doesHave_ControlledProperty_notSet($errorPrefix . $dtoValid->message);

//            } else
            if ($dtoValid->enumReason == 'LogicError') {
                throw ErrorFromCfd::LogicError($errorPrefix . $dtoValid->message);
            } elseif ($dtoValid->enumReason == 'reducedToEmpty') {
                throw ErrorFromCfd::LogicError($errorPrefix . "{$dtoValid->enumReason}: " . $dtoValid->message);
//            } else if ($dtoValid->enumReason == 'doesHave_notSubmitted') {
//                throw ErrorFromCfd::doesHave_ControlledProperty_notSet($errorPrefix . "{$dtoValid->enumReason}: " . $dtoValid->message);

            } else {
//                if ( $asrRichProperties['_meta']['className'] == 'SchoolTwist\Cfd\Core\CfdRichProperty') {
//                    print "<br><pre>";
//                    print_r([__FILE__, __LINE__, $asrRichProperties, $asrSubmission]);
//                    print "</pre>";
//                    \EtError::AssertTrue(0,'error',__FILE__,__LINE__,'');
//
//                }
                throw ErrorFromCfd::LogicError($errorPrefix . "Default error for {$dtoValid->enumReason} " . $dtoValid->message);
            }
        }
        $asrSubmissionNormalized = $dtoValid->newValue;
        unset($asrSubmission);


        $objRef = new ReflectionClass($this);
        #$arrStaticProperties = $objRef->getProperties(ReflectionProperty::IS_STATIC);
//        print "<br>".__FILE__.__LINE__.__METHOD__."<br>"."<pre>";
//        print_r($asrSubmission);
//        print "</pre>";


        foreach ($asrSubmissionNormalized as $submittedPropertyName => $submittedValue) {
            //if ()
            $isStatic = $objRef->getProperty($submittedPropertyName)->isStatic() ? 1 : 0 ;
            $typeOfSubmittedValue = gettype($submittedValue);
            foreach ($asrRichProperties['properties'][$submittedPropertyName]['types'] as $expectedType) {
                #$expectedType = $asrRichProperties['properties'][$submittedPropertyName]['type'];
                $oldType = gettype($submittedValue);


                $isFinalType = (gettype($submittedValue) == 'object') ? (get_class($submittedValue) == $expectedType) : (gettype($submittedValue) == $expectedType); // old gettype logic - 'object' is obe 12/20'
                if (!$isFinalType) {
                    $nameOfCfdBase = CfdBase::class;
                    $isDestinedForCfd = is_a($expectedType, $nameOfCfdBase, true) ? 1 : 0;
                    if ($isDestinedForCfd) {





                        #print __LINE__ . ") Not final type -  going deeper for submittedPropertyName($submittedPropertyName) ";
                        $dtoValidUpconvert_elseNullIfDone = $expectedType::upConvertType_elseNull($submittedPropertyName, $submittedValue);
                        if ($dtoValidUpconvert_elseNullIfDone && $dtoValidUpconvert_elseNullIfDone->isValid) {
                            $upConvertedValue_elseNullIfDone = $dtoValidUpconvert_elseNullIfDone->newValue;
                            $newType = gettype($upConvertedValue_elseNullIfDone);

                            if ($upConvertedValue_elseNullIfDone) {
                                $submittedValue_old = $submittedValue;
                                $submittedValue = $upConvertedValue_elseNullIfDone;


                                if ($newType == 'object') {
                                    $newType = get_class($upConvertedValue_elseNullIfDone);
                                }

                            }
                        }

                    } else {
                        // logic error?  I think maybe
                        $typeOfSource_shortendName = static::deriveShortNameFromCfdClassName_givenFqn($typeOfSubmittedValue);
                        $upconvertingMethodName = "from_{$typeOfSource_shortendName}";
                        $typeItShouldBeOrdAncestorOfThis_Name = $expectedType;
                        $worthCheckingForMethods = class_exists($typeItShouldBeOrdAncestorOfThis_Name) ;

                        if ($worthCheckingForMethods && method_exists($typeItShouldBeOrdAncestorOfThis_Name, $upconvertingMethodName)) {
                            $convertedValue = $typeItShouldBeOrdAncestorOfThis_Name::$upconvertingMethodName($submittedValue, false);
                            $this->$submittedPropertyName = $convertedValue;
                            break;
                        }
                    }
                }

                if ($isStatic) {
                    $this::$$submittedPropertyName = $submittedValue; //2/20'  Hmm - is this ever really a thing?  WHy here?  This breaks round-tripping of toArray since that doesn't export everything, like 'int_version'
                } else {
                    $this->$submittedPropertyName = $submittedValue;
                }
            }

        }
        #\ClsProbe::singleton()->stop(__METHOD__, $ut);
    }


    public static function preValidateSubmission($asrSubmissionOrObject_hmmNotObject): \SchoolTwist\Validations\Returns\DtoValid
    {   /* Remember: All properties must be set to something, even if it is null
        */
        #$ut = \ClsProbe::singleton()->start(__METHOD__,'');
        $asr_RichProperties = static::getCfdRichProperties();

        $asrRichProperties = $asr_RichProperties['properties'];
        $asrSubmission = $asrSubmissionOrObject_hmmNotObject;

        // ----------------------------------- simple check ------------------------------------------------------------
        $arrPropertiesChecked = [];
        foreach ($asrSubmission as $submittedPropertyName => $proposedValue) {
            $dtoValid = static::preValidateProperty($submittedPropertyName, $proposedValue, null);
            if (!$dtoValid->isValid) {
                return $dtoValid;
            }
            $arrPropertiesChecked[$submittedPropertyName] = $submittedPropertyName;

        }
        unset($submittedPropertyName);
        // Are any required properties missing?

        foreach ($asr_RichProperties['properties'] as $propertyName => $asr_RichProperty) {
            if (!isset($arrPropertiesChecked[$propertyName])) {
                if ($asr_RichProperty['isRequired']) {

                    return new \SchoolTwist\Validations\Returns\DtoValid(
                        [
                            'isValid' => false,
                            'enumReason' => 'missingProperty',
                            'message' => " Missing '$propertyName'  '" . static::class . "'",

                        ]
                    );
                }
            }
        }






        // ----------------------------------- notExtras ---------------------------------------------------------------
        foreach ($asrSubmission as $submittedPropertyName => $submittedValue) {
            if (!isset($asr_RichProperties['properties'][$submittedPropertyName])) {
                if (!property_exists(get_called_class(),$submittedPropertyName)){

                    return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'enumReason' => 'extraData', 'message' => " '$submittedPropertyName' is not an allowed property for '" . static::class . "'"]);

                }
            }
        }


        // ----------------------------------- doesHave_ 1 of 2 --------------------------------------------------------
        // if starts with 'doesHave' then make sure:
        //  - that the only two options are 'bool' or 'bool|null'
        //  - there is a correspdoning deferenced var
        //  - if set to a boolean (vs. null) that it matches the truth of the derefences var, mainly, true if
        //      the other one is not null, and false if the other one is null.
        //  - if it is set to null, then automatically set it to proper state
        // State: it will be set to something (maybe null if that was specified as an option)

        // 8/20' This was a bad idea. Simply use the null convention.

        //        $prefix = 'doesHave_';  // Must be specked to boolean, or boolean|auto
        //                                // If 'auto', then it is set automatically if unset, or set to null.
        //        foreach ($asrRichProperties as $speckedPropertyName => $proposedValue) {
        //            $asrRichProperty = $asrRichProperties[$speckedPropertyName];
        //
        //            if (static::StartsWith($prefix, $speckedPropertyName)) {
        //                // meets spec 'boolean' or 'boolean|null'
        //                if (count(array_intersect(['bool'], $asrRichProperty['types'])) != 1) {
        //                    return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'enumReason' => 'doesHave_NotSpeckedTo_Boolean', 'message' => " '$speckedPropertyName' must be specked to have type boole (and maybe null): " . static::class . "'"]);
        //                }
        //
        //                // State: this is a bool, maybe with optional null.
        //                $doesThisHaveNullAsAnOption = $asrRichProperty['isNullAnAllowedType'];
        //                $dereferenced_property_name = static::TrimOffFront($prefix, $speckedPropertyName);
        //                $is_dereferenced_property_name_specked_for_this_class = isset($asrRichProperties[$dereferenced_property_name]);
        //
        //                // There must be a correcsponding property
        //                if (!$is_dereferenced_property_name_specked_for_this_class) {
        //                    return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'enumReason' => 'doesHave_dangles', 'message' => " '$speckedPropertyName' must reference another property, namely '$dereferenced_property_name' is missing: " . static::class . "'"]);
        //                }
        //
        //                $amISet_ish = (isset($asrSubmissionOrObject[$speckedPropertyName]) && !is_null($asrSubmissionOrObject[$speckedPropertyName])) ? 1 : 0;
        //                $isDereferenceProperty_set_ish = (isset($asrSubmissionOrObject[$dereferenced_property_name]) && !is_null($asrSubmissionOrObject[$dereferenced_property_name])) ? 1 : 0;
        //
        //                // - is the other var non-null?
        //                #$asrRichProperty_ofDereferencedProperty = $asrRichProperties[$dereferenced_property_name];
        //                #$isDereferenceProperty_setTo_null = is_null($asrSubmissionOrObject[$dereferenced_property_name]);
        //                // ensure I'm set unless I have null as an option.
        //                if (!$doesThisHaveNullAsAnOption) {
        //                    if (!$amISet_ish) {
        //                        return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'enumReason' => 'doesHave_notSet', 'message' => " '$speckedPropertyName' must be set to true or false, unless it is specked to have null as an option. '$dereferenced_property_name' is missing: " . static::class . "'"]);
        //                    }
        //                }
        //
        //                // If I'm not set, then set me automatically.
        //                if (!$amISet_ish) {
        //                    $asrSubmissionOrObject[$speckedPropertyName] = ($isDereferenceProperty_set_ish) ? 1 : 0;
        //                }
        //
        //                // Doublecheck that I am set properly
        //                $iDoesHave = $asrSubmissionOrObject[$speckedPropertyName];
        //                if ($amISet_ish != $isDereferenceProperty_set_ish) {
        //                    $isIsNotSet = $isDereferenceProperty_set_ish ? 'is set' : 'is not set';
        //                    return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'enumReason' => 'doesHave_doesNotMatch', 'message' => " '$speckedPropertyName($iDoesHave)' references '$dereferenced_property_name', but '$dereferenced_property_name' $isIsNotSet: " . static::class . "'"]);
        //                }
        //
        //
        //
        //            }
        //        }



        $r = new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => true, 'newValue'=>$asrSubmission, 'oldValue'=>$asrSubmissionOrObject_hmmNotObject]);
        #\ClsProbe::singleton()->stop(__METHOD__,$ut); //
        return $r;
    }

    public
    static function preValidateLoneValue($dangerousValue, ?bool $isRequired_ifSet_otherwise_requireAsConfiggedWhenNull, array $callingRichProperties): \SchoolTwist\Validations\Returns\DtoValid
    {
        #$ut = \ClsProbe::singleton()->start(__METHOD__,'');
        $asr_RichProperties = static::getCfdRichProperties();

        if (count($asr_RichProperties['properties']) != 1) {


            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'enumReason' => 'onlyLoneSubValuesAllowed', 'message' => get_called_class() . ": Composed DTOs can only have a single value unless they are subclasses of CFDs, like DtoEmail just has a single value for its email address. " . __LINE__]);
        }
        $propertyName = array_key_first($asr_RichProperties['properties']);
        $r = static::preValidateProperty($propertyName, $dangerousValue, $isRequired_ifSet_otherwise_requireAsConfiggedWhenNull);
        #\ClsProbe::singleton()->stop(__METHOD__,$ut);
        return $r;
    }


    /* Just to clarify: 8/20'
        null is allowed, when specked to allow null.
        is $mustBeInitializedToNonNull_ifSet_otherwise_requireAsConfiggedWhenNull == true, then it must be non-null.
        When we say it mustBeInitialized, it probably means we don't have a default value.
        so, isRequiredNow = mustBeInitialized || $mustBeInitializedToNonNull_ifSet_otherwise_requireAsConfiggedWhenNull
        Caution: the string '' is considered set.

        Flow Note: since this is preValidateProperty, it is logical to require a value to test.  Specked Default values just aren't relevant here.

       This is mostly redundant to other validation code. Sigh.

    I think isRequired is flotsam 12/14/20' but maybe not
    */

    public
    static function preValidateProperty($propertyName, $dangerousValue, ?bool $mustBeInitializedToNonNull_ifSet_otherwise_requireAsConfiggedWhenNull): \SchoolTwist\Validations\Returns\DtoValid
    {
        #$ut = \ClsProbe::singleton()->start(__METHOD__,'');
        // $isRequired might be a misnomer post 8/20'  Think isTracked vs. mustInitialize.
        //      Items with defaults don't need to be manually initialized.
        $asr_RichProperties = static::getRichProperties();
        $asrRichProperties = $asr_RichProperties['properties'];
        $asrMeta = $asr_RichProperties['_meta'];
        $isAProperty = isset($asrRichProperties[$propertyName]);
        if (!$isAProperty) {
            $r = new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'oldValue'=>$dangerousValue, 'newValue'=>$dangerousValue, 'enumReason' => 'notARealOption', 'message' => "'$propertyName' is not a valid property for class " . get_called_class().". Hint: Sometimes you see this when you pass a list instead of associative array"]);
            #\ClsProbe::singleton()->stop(__METHOD__,$ut);
            return $r;
        }
        $isNonMeta = !$asrRichProperties[$propertyName]['isMeta'];


        $mustBeInitialized_origSoIgnoresOverride_zo = $asrRichProperties[$propertyName]['mustBeInitialized'] ? 1 : 0;

        $mustBeSetToNonNullNow_zo = is_null($mustBeInitializedToNonNull_ifSet_otherwise_requireAsConfiggedWhenNull) ? 0 : 1;
        $isRequiredNow_OrNotDoNotUseMe_zo = ($asrRichProperties[$propertyName]['mustBeInitialized'] || $mustBeSetToNonNullNow_zo) ? 1 : 0;
        #$didRequiredStateChange_zo = ($isRequiredNow_OrNotDoNotUseMe_zo != $mustBeInitialized_origSoIgnoresOverride_zo) ? 1 : 0;



        $typeOfSubmittedValue = gettype($dangerousValue);



        // 8/20' why isn't integer int here?
        $typeOfSubmittedValue = ($typeOfSubmittedValue == 'boolean') ? 'bool': $typeOfSubmittedValue;
        $typeOfSubmittedValue = ($typeOfSubmittedValue == 'NULL') ? 'null' : $typeOfSubmittedValue;
        $setButEmpty = ($typeOfSubmittedValue == 'string') ? (strlen(trim($dangerousValue)) == 0) : 0; //  Careful: empty('0') evaluates to true

        // if null, is it legal?
        //  Hmm - seems like I should be trying to upconvert, first? 8/20'
        if ($typeOfSubmittedValue == 'null') {
             $isNullAnAllowedType =  $asrRichProperties[$propertyName]['isNullAnAllowedType'];
             if (!$isNullAnAllowedType) {
                 $debugClassName = get_called_class();
                 return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'oldValue'=>$dangerousValue, 'newValue'=>$dangerousValue, 'enumReason' => 'setToNullWhenNotSpecked', 'message' => "$debugClassName->$propertyName' is '$typeOfSubmittedValue' but null isn't specked option."]);
             } else {
                 // $isNullAnAllowedType == false
                 if ($mustBeInitializedToNonNull_ifSet_otherwise_requireAsConfiggedWhenNull) {
                     $debugClassName = get_called_class();
                     return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'oldValue'=>$dangerousValue, 'newValue'=>$dangerousValue, 'enumReason' => 'setToNullWhenSpeckedButDirectedToMustBeNonNull', 'message' => "$debugClassName->$propertyName' is '$typeOfSubmittedValue' but null, which would otherwise be ok, but arguments dictate that it must now be non-null at validation."]);
                 }
             }

        }



        $nonEmpty = !$setButEmpty;

        // ensure matching type
        // If this is another Cfd_Base, run validation on it
        // if multiple types, stop on first upconvert... so property order matters

        foreach ($asrRichProperties[$propertyName]['types'] as $typeItShouldBeOrdAncestorOfThis_Name) {
            // 12/20' misnomer? $typeItShouldBeOrdAncestorOfThis_Name ==> $typeItShouldBeOrDescendantOfThis_Name
            $isNonTrackedVar = $asrRichProperties[$propertyName]['isTypeEnforced'] ? 0 : 1;
            $isVarNullAndThatIsOk = !$isNonTrackedVar && ($asrRichProperties[$propertyName]['isNullAnAllowedType'] && ($typeOfSubmittedValue == 'null')) ? 1 : 0;
            $doesTypeMatchDirectly = !$isNonTrackedVar && in_array($typeOfSubmittedValue, $asrRichProperties[$propertyName]['types']);
            $doesTypeMatchViaObject = null;


            if (!$doesTypeMatchDirectly) {
                $typeOfSubmittedValue =  gettype($dangerousValue);// 12/20' I think get type is good enough now in 7.4get_class($dangerousValue) ? get_class($dangerousValue) : gettype($dangerousValue); //
                $doesTypeMatchViaObject = is_a($dangerousValue, $typeItShouldBeOrdAncestorOfThis_Name) ? 1 : 0;
                if (!$doesTypeMatchViaObject) {
                    $doesTypeMatchViaObject = ($dangerousValue instanceof  $typeItShouldBeOrdAncestorOfThis_Name) ? 1 : 0;

                }
                // Look above at in_array.  If we really start having multiple allowed types, this will have to match
            }

            // If not matching yet - try upconverting
            $doesTypeMatchViaUpConverting = null;
            $doesTypeMatchViaUpConverting_viaAncestry = null;
            $doesTypeMatchViaInterface = null;
            $doesTypeMatchViaUpConverting_explicitViaList = 0;
            $doesTypeMatchViaUpConverting_stringToInt = 0;
            if (!$doesTypeMatchDirectly && !$doesTypeMatchViaObject) {

                $nameOfCfdBase = CfdBase::class;
                $isDestinedForCfd = is_a($typeItShouldBeOrdAncestorOfThis_Name, $nameOfCfdBase, true) ? 1 : 0;

                if (!$isDestinedForCfd) {

                    $doesTypeMatchViaUpConverting = 0;

                    $upconvertingMethodName = "from_{$typeOfSubmittedValue}";
                    if (method_exists($typeItShouldBeOrdAncestorOfThis_Name, $upconvertingMethodName)) {
                        $doesTypeMatchViaUpConverting = 1;
                        #break;
                    }

                    $doesTypeMatchViaInterface = ($dangerousValue instanceof $typeItShouldBeOrdAncestorOfThis_Name) ? 1 : 0;// class_implements($typeOfSubmittedValue, $typeItShouldBeOrdAncestorOfThis_Name, true) ? 1 : 0;

                    if (!$doesTypeMatchViaInterface) {
                        if ($typeItShouldBeOrdAncestorOfThis_Name == 'int' && $typeOfSubmittedValue == 'string' && is_numeric($dangerousValue) && is_int($dangerousValue*1)) {
                            $doesTypeMatchViaUpConverting_stringToInt = 1;
                            $dangerousValue = $dangerousValue*1;
                        }
                    }

                } else {
                    $dtoValidIfConversionAllowed_elseNullNotAttempted = $typeItShouldBeOrdAncestorOfThis_Name::upConvertType_elseNull($propertyName, $dangerousValue, $mustBeInitializedToNonNull_ifSet_otherwise_requireAsConfiggedWhenNull);
                    $typeOfSubmittedValue_old = $typeOfSubmittedValue;

                    if (!$dtoValidIfConversionAllowed_elseNullNotAttempted) {

                        $doesTypeMatchViaUpConverting = false;

                    } elseif (!$dtoValidIfConversionAllowed_elseNullNotAttempted->isValid) {
                        // I should be able to upconvert, but there was a validation error - like bad data, not bad typing
                        return $dtoValidIfConversionAllowed_elseNullNotAttempted;

                    } elseif ($dtoValidIfConversionAllowed_elseNullNotAttempted->isValid) {
                        $newItem = $dtoValidIfConversionAllowed_elseNullNotAttempted->newValue;

                        // Update typing info/logic
                        $typeOfSubmittedValue_upconverted = gettype($newItem);
                        $typeOfSubmittedValue_upconverted = ($typeOfSubmittedValue_upconverted == 'object') ? get_class($newItem) : $typeOfSubmittedValue;

                        $doesTypeMatchViaUpConverting = ($typeOfSubmittedValue_upconverted == $typeItShouldBeOrdAncestorOfThis_Name) ? 1 : 0;
                        if (!$doesTypeMatchViaUpConverting && (gettype($newItem) == 'object')) {
                            $doesTypeMatchViaUpConverting_viaAncestry = is_a($newItem, $typeOfSubmittedValue_upconverted) ? 1 : 0;
                            // Look above at in_array.  If we really start having multiple allowed types, this will have to match
                        }
                        $dangerousValue = $newItem;

                    } else {
                        throw new \Exception('logic error');
                    }


                }


            }

            // if we match --> stop cycling through types
            if ($isNonTrackedVar || $isVarNullAndThatIsOk || $doesTypeMatchDirectly || $doesTypeMatchViaObject || $doesTypeMatchViaUpConverting || $doesTypeMatchViaUpConverting_viaAncestry || $doesTypeMatchViaUpConverting_explicitViaList || $doesTypeMatchViaInterface || $doesTypeMatchViaUpConverting_stringToInt) {

                // ensure validates by custom function, if there.  Not sure if at top or bottom, or both
                // could, if needed in the future, see if match on various ways and validate against each, but probably just asking for trouble
                $validatingMethodName = "{$propertyName}_Validates";
                if ($nonEmpty && method_exists(static::class, $validatingMethodName)) {
                    $DtoValidation = static::$validatingMethodName($dangerousValue);
                    $meClass = get_called_class();
                    $static_class = static::class;
                    $dtoValid_zo = $DtoValidation->isValid ? 1 : 0;
//                return new \SchoolTwist\Validations\Returns\DtoValid([
//                    'isValid'=>false, 'enumReason'=>
//                        'JJCanNotProgram'. "meClass($meClass) static_class($static_class) dtoValid_zo($dtoValid_zo) validatingMethodName($validatingMethodName) dangerousValue($dangerousValue)"]);

                    if (!$DtoValidation->isValid) {

                        #&& in_array($DtoValidation->enumReason, ['setToNullWhenRequired', 'reducedToEmpty']) ) {
                        return $DtoValidation;
                    }
                }


                // matches
                $asrReasons = compact([ $isNonTrackedVar, $isVarNullAndThatIsOk, $doesTypeMatchDirectly, $doesTypeMatchViaObject, $doesTypeMatchViaUpConverting, $doesTypeMatchViaUpConverting_viaAncestry]);
                foreach ($asrReasons as $key=>$val) {
                    if (!$val) {
                        unset($asrReasons[$key]);
                    }
                }
                $csvReasons = implode(',',array_keys($asrReasons));
                return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => true, 'enumReason' => $csvReasons, 'newValue'=>$dangerousValue, 'oldValue'=>$dangerousValue]);
            }

        }










        $meTYpe = get_called_class();
        $doesTypeMatchViaUpConverting = $doesTypeMatchViaUpConverting ? 1 : 0;

        $doesTypeMatchViaUpConverting_explicitViaList = in_array($typeOfSubmittedValue,static::ALLOW_UPCONVERT_TYPES) ? 1 : 0;


        return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'enumReason' => 'wrongType',  'oldValue'=>$dangerousValue, 'newValue'=>$dangerousValue,'message' => " $meTYpe: '$propertyName' must be of a type (" . implode(',', $asrRichProperties[$propertyName]['types']) . ") but received '$typeOfSubmittedValue'. doesTypeMatchViaUpConverting($doesTypeMatchViaUpConverting)->for(".implode(',',static::ALLOW_UPCONVERT_TYPES).") doesTypeMatchViaUpConverting_local($doesTypeMatchViaUpConverting_explicitViaList) " . __METHOD__.__LINE__]);


//        // ensure validates by custom function, if there.  Not sure if at top or bottom, or both
//        $validatingMethodName = "{$propertyName}_Validates";
//        if ($nonEmpty && method_exists(static::class, $validatingMethodName)) {
//            $DtoValidation = static::$validatingMethodName($dangerousValue);
//            if (!$DtoValidation->isValid) {
//
//                #&& in_array($DtoValidation->enumReason, ['setToNullWhenRequired', 'reducedToEmpty']) ) {
//                return $DtoValidation;
//            }
//        }


    }


    public static function upConvertType_elseNull(string $propertyName, $dangerousValue) : ?\SchoolTwist\Validations\Returns\DtoValid
    {
        // fun fact: you are probably now in a different subtype than you were a step ago - don't get confused
        // You can override me if desired
        // Note: if null, it means we are not even going to try to upconvert.
        //  otherwise, we get a DtoValid.  If it is valid, newValue will contain the upconverted type
        $incomingType = gettype($dangerousValue);
        if (!in_array($incomingType,static::ALLOW_UPCONVERT_TYPES)){
            return null;
        }
        if ($incomingType == 'integer' || $incomingType == 'string') {
            $childType = get_called_class();
            $dtoValid = $childType::preValidateProperty('Value',$dangerousValue, null);
            if (!$dtoValid->isValid) {
                return $dtoValid;
            } else {
                $newCfd = new $childType(['Value' => $dangerousValue]);
                $dtoValid =  new \SchoolTwist\Validations\Returns\DtoValid(['isValid'=>true,'enumReason'=>'itPassed', 'newValue'=>$newCfd]);
                return $dtoValid;
            }
        } else {
            return null;
        }
    }


    public function getTerseRichCfdProperties_withValues() : array {
        $asrRichPropreties = static::getRichProperties();
        $asrCfdProperties = [];
        foreach ($asrRichPropreties['properties'] as $propertyName=>$asrRichProperty) {
            if ($asrRichProperty['isTypeEnforced']) {
                $asrCfdProperties[$propertyName] = [
                    'shortPropertyName '=> $propertyName,
                    'isNull'=> is_null($this->$propertyName),
                    'Value'=> $this->$propertyName
                    ];
            }
        }
        return $asrCfdProperties;
    }


    public
    static function getCfdRichProperties(): array
    {
        $asrRichPropreties =  static::getRichProperties_ofDtoNamed(static::class);

        foreach ($asrRichPropreties['properties'] as $propertyName=>$asrRichProperty) {
            if ($asrRichProperty['isTypeEnforced']) {
            } else {
                unset($asrRichPropreties['properties'][$propertyName]);
                // Meta is probably wrong now
            }
        }

        return $asrRichPropreties;
    }
    /* Motivation: when trying to get out of the array habbit */

    public static function getCfdRichProperties_filteredAndUpRequired(array $arrOnlyKeepTheseProperties, array $arrPropertyNames_upForcingToRequired): array
    {
        $DtoCfdSingleFormName = get_called_class();
        $asrBlade['DtoCfdSingleFormName'] = $DtoCfdSingleFormName;

        // Put together data
        $arrNamesForcedToBeRequired_evenIfNotRequiredByDefault = $arrPropertyNames_upForcingToRequired;
        $DtoCfdClassName = $DtoCfdSingleFormName;
        $richProperties = $DtoCfdClassName::getCfdRichProperties($DtoCfdClassName);


        foreach ($arrNamesForcedToBeRequired_evenIfNotRequiredByDefault as $nameToForceStatusToRequired) {
            $richProperties['properties'][$nameToForceStatusToRequired]['mustBeInitialized'] = 1;
        }
        $asrAsrRichProperties = $richProperties;


        $orderedButMissingFields = array_diff($arrOnlyKeepTheseProperties, array_keys($asrAsrRichProperties['properties']));
        $skippedFields = array_diff(array_keys($asrAsrRichProperties['properties']), $arrOnlyKeepTheseProperties);
        // OBE: Not All Fields automatically get presented to user $fieldOrder = array_merge($fieldOrder, $unorderedFields);


        $asrBlade['effectiveProperties'] = $asrAsrRichProperties;//$DtoCfdSingleFormName::getRichProperties();
        // Nix the non-user properties - note: This file needs to merge with Auth/forSingleCfd... 1/20'
        foreach ($asrBlade['effectiveProperties']['properties'] as $propertyName => $effectiveProperty) {
            #print "<br>--- Checking on propertyName($propertyName) to see if in field order";
            if (!(in_array($propertyName, $arrOnlyKeepTheseProperties))) {
                unset($asrBlade['effectiveProperties']['properties'][$propertyName]);
                #print "<br> - MISS:  About to unset $propertyName";
            } else {
                #print "<br> - HIT:   $propertyName is a legit property";
            }
        }
        $asrCfdRichProperty = [];

        foreach ($asrBlade['effectiveProperties']['properties'] as $asrRichProperty) {
            $cfdCfdRichProperty = static::asrRichProperty2CfdRichProperty($asrRichProperty);
            $asrCfdRichProperty[$cfdCfdRichProperty->name] = $cfdCfdRichProperty;
        }
        return $asrCfdRichProperty;
    }


    public static function asrRichProperty2CfdRichProperty($asr): \SchoolTwist\Cfd\Core\CfdRichProperty {


        return new \SchoolTwist\Cfd\Core\CfdRichProperty([
            'isStatic' => $asr['isStatic'] ? true : false,
            'isMeta' => $asr['isMeta'] ? true : false,
            'name' => $asr['name'],
            #'hasStaticDefault' => $asr['hasStaticDefault'] ? true : false,
            #'hasADocComment' => $asr['hasADocComment'] ? true : false,
            #'docCommentForThisProperty' => $asr['docCommentForThisProperty'],
            'isTypeEnforced' => $asr['isTypeEnforced'] ? true : false,                      // aka: is Managed
            'isNullAnAllowedType' => $asr['isNullAnAllowedType'] ? true : false,
            'mustBeInitialized' => $asr['mustBeInitialized'] ? true : false,
            'type' => $asr['type'],
            'types' => $asr['types'],
            'hasDefault' =>$asr['hasDefault'],
            'default' =>$asr['default'],
            'isRequired' =>$asr['isRequired'],
            ]);
    }

    /*
     * isTypeEnforced: aka is a managed property
     * mustBeInitialized: do I need this set, like right now. This is true if isTypeEnforced and no default
     * hasDefault: The programmer set a default value, which also implies mustBeInitialized is always false
     * isRequired: an extra layer. The forms might do some soft logic that make this field required, like when
     *              age <18, then legal-gaurdian becomes required (or something like that)
     *              This is mostly for forms and not totally sure it makes sense here.
     */
    public
    static function getRichProperties(): array
    {
        return static::getRichProperties_ofDtoNamed(static::class);
    }

    private static array $_cachedRichProperties = [];
    private
    static function getRichProperties_ofDtoNamed(string $CfdClassName): array
    {
        #$ut = \ClsProbe::singleton()->start(__METHOD__,'');
        #$utc = \ClsProbe::singleton()->start(__METHOD__.'/'.$CfdClassName,'');
        if (isset(static::$_cachedRichProperties[$CfdClassName])) {
            #\ClsProbe::singleton()->stop(__METHOD__.'/'.$CfdClassName,$utc);
            #\ClsProbe::singleton()->stop(__METHOD__,$ut);
            return static::$_cachedRichProperties[$CfdClassName];
        }
        $reflectionClass = new ReflectionClass($CfdClassName);
        $asrDefaultProperties = $reflectionClass->getDefaultProperties();

        $richProperties = [];



        $numNonMeta = 0;
        foreach ($reflectionClass->getProperties(ReflectionProperty::IS_PUBLIC) as $reflectionProperty) {
            #$utf = \ClsProbe::singleton()->start(__METHOD__.'/foreach','');
            $propertyName = $reflectionProperty->getName();
            #$utcf = \ClsProbe::singleton()->start(__METHOD__."/foreach/$CfdClassName/$propertyName",'');
            // isRequired
            // isMeta
            // hasDefault (tbd)





            $richProperties[$propertyName] = [];
            $richProperties[$propertyName]['name'] = $propertyName;

            // Is Static
            $isStatic_zo = $reflectionProperty->isStatic() ? 1 : 0 ;//
            $isStatic_zo = $reflectionClass->getProperty($propertyName)->isStatic() ? 1 : 0 ;
            $richProperties[$propertyName]['isStatic'] = $isStatic_zo;

            // Is Meta
            $richProperties[$propertyName]['isMeta'] = 0;
            foreach (static::META_PREFIXES as $metaPrefix) {
                if (strpos($propertyName, $metaPrefix) === 0) {
                    $richProperties[$propertyName]['isMeta'] = 1;
                    break;
                }
            }

            if ($richProperties[$propertyName]['isMeta'] == false) {
                foreach (static::META_PROPERTY_NAMES as $metaPropertyName) {
                    if ($metaPropertyName == $propertyName) {
                        $richProperties[$propertyName]['isMeta'] = 1;
                    }
                }
            }
            $numNonMeta = $numNonMeta + !$richProperties[$propertyName]['isMeta'];
            #$richProperties['numNonMeta']=$numNonMeta;

            // Handle default values
            //  static
            #dd($asrDefaultProperties);
#            $doesHaveDefaultValueSet = key_exists($propertyName, $asrDefaultProperties); // nixed cause it doesn't catch null as default isset($CfdClassName::$$propertyName);
            //            if ($doesHaveDefaultValueSet) {
            //                $richProperties[$propertyName]['hasStaticDefault'] = 1;
            //                $richProperties[$propertyName]['staticDefault'] = $CfdClassName::$$propertyName;
            //            } else {
            //                $richProperties[$propertyName]['hasStaticDefault'] = 0;
            //                $richProperties[$propertyName]['staticDefault'] = null;
            //            }
            // nonstatic

            if (!isset($asrDefaultProperties[$propertyName])) {
//                print_r($asrDefaultProperties);
//                exit;
                #throw  ErrorFromCfd::LogicError("Default error for ".get_called_class()."::$propertyName. $propertyName is a public property, but seemingly not given a type.  Make it look something like: public int \$name;");
            }
            $richProperties[$propertyName]['hasDefault'] = $doesHaveDefaultValueSet = key_exists($propertyName, $asrDefaultProperties);
            $richProperties[$propertyName]['default'] = (!isset($asrDefaultProperties[$propertyName]) || is_null($asrDefaultProperties[$propertyName])) ? null : $asrDefaultProperties[$propertyName];

//             $doesHaveDefaultValueSet = key_exists($propertyName, $asrDefaultProperties) ? 1 : 0;
            $richProperties[$propertyName]['hasDefault'] = $doesHaveDefaultValueSet;
            $richProperties[$propertyName]['default'] = ($doesHaveDefaultValueSet) ? $asrDefaultProperties[$propertyName] : 'no default set';
//if ($propertyName == 'payload') {
//                dd([$doesHaveDefaultValueSet, $asrDefaultProperties, $richProperties]);
//            }
            // Is Required? (all non-static public properties w/o a default)
            $isPublic = $reflectionProperty->isPublic()? 1 : 0;
            $hasDefault = $doesHaveDefaultValueSet;
            $hasType = $reflectionProperty->hasType() ? 1 : 0;
            $isTypeEnforced = $isPublic && !$isStatic_zo;
            $mixed_isExplicitlySpeckedAsAType = false;
            $hasManyAllowedExplicitTypes = false; // Logic will need to change once php 8 is here. In 7.4, union types are only available in the docblock as a fallback, and will only be seen if now php property type is specified.
            if ($isTypeEnforced && !$hasType) {
                // We'll still rely (fallback to) on the docblock until php 8, mainly catching 'mixed' cuz some things really ~need it, namely 'default' and 'pk'
                $docCommentForThisProperty = $reflectionClass->getProperty($propertyName)->getDocComment();
                preg_match('/\@var ((?:(?:[\w|\\\\])+(?:\[\])?)+)/', $docCommentForThisProperty, $matches);
                $hasAtVarDocComment = (count($matches));
                $varDocComment = end($matches);
                $richProperties[$propertyName]['dtoDocComment'] = $varDocComment;
                $arrTypes = array_map('trim', explode('|', $varDocComment));// explode('|', $varDocComment);
                $arrTypes_withoutBrackets = str_replace('[]', '', $arrTypes); // nix me



                // Note: This section  is a hold-over for docblock days. We're living in php 7.4's property types, but and waiting for union types and mixed types in php 8
                //  Of simplicity, we've only implemented 'mixed' type via docblock instead of union types since there is only one suspected use case (CfdbUuidTbl).... and even then, we're not sure.
                // is type 'mixed'?
                $mixed_isExplicitlySpeckedAsAType = (array_search('mixed',$arrTypes_withoutBrackets) === false) ? false : true;
                $hasManyAllowedExplicitTypes = count($arrTypes_withoutBrackets) > 1;

                if ($hasManyAllowedExplicitTypes) { // misnomer, 'null' might be in this list
                    $isNullable_indicatingSlotOrFalse = (array_search('null', $arrTypes_withoutBrackets));
                    if ($isNullable_indicatingSlotOrFalse !== false) {
                        unset($arrTypes_withoutBrackets[$isNullable_indicatingSlotOrFalse]);
                        $isNullable = true;
                    }
                    $hasManyAllowedExplicitTypes = count($arrTypes_withoutBrackets) > 1;
                } // output: $hasManyAllowedExplicitTypes - 'null' will no longer be in this list
                if ($hasManyAllowedExplicitTypes ) {
                    if ($mixed_isExplicitlySpeckedAsAType) {
                        throw ErrorFromCfd::LogicError("This " . get_called_class() . "->$propertyName ($varDocComment) Our logic for union types only allows for 'mixed' or | separated types in the doc string (until we migrate to php 8 and upgrade cfd). Please use one approach (single php types next to property, like 'public int \$i', or union types in the doc string, like '@var int|string|null' , or the other. 
                            <br>mixed_isExplicitlySpeckedAsAType($mixed_isExplicitlySpeckedAsAType),
                            <br>hasManyAllowedExplicitTypes($hasManyAllowedExplicitTypes)
                        ");
                    }
                }


                if ($mixed_isExplicitlySpeckedAsAType) {

                    $isTypeEnforced = 0;
                } elseif ($hasManyAllowedExplicitTypes) {

                } else {
                    throw ErrorFromCfd::LogicError("This " . get_called_class() . "->$propertyName is non-static and public, but does not have its type defined.  All non-static public properties for a CFD must have a property type specified. Hint: If you want a 'mixed' type, add it to doc block ");
                }
            }

            $doesImplementDeStringable = 0;
            $richProperties[$propertyName]['isTypeEnforced'] = $isTypeEnforced ? 1 : 0;
            if (!$isTypeEnforced) {
                $richProperties[$propertyName]['mustBeInitialized'] = 0;
                $richProperties[$propertyName]['types'][] = 'mixed';
                $richProperties[$propertyName]['isNullAnAllowedType'] = 1;
            } else {
                $mustBeInitialized = $isPublic && !$hasDefault;
                $richProperties[$propertyName]['mustBeInitialized'] = $mustBeInitialized ? 1 : 0;
                if ($mixed_isExplicitlySpeckedAsAType) {
                    // hack - we've reverting to docblock for 'mixed' type
                    $typeIsAsString = 'mixed';
                    $isNullable = (array_search('null', $arrTypes_withoutBrackets) === false ? false : true);
                }elseif ($hasManyAllowedExplicitTypes) {
                        // hack - we've reverting to docblock for union types
                        $typeIsAsString = 'union - make better in php 8';

                        $isNullable = (array_search('null', $arrTypes_withoutBrackets) === false ? false : true);

                } else {
                    if (!$reflectionProperty) {throw ErrorFromCfd::LogicError("1reflectionProperty is not there ");}
                    if (!$reflectionProperty->getType()) {throw ErrorFromCfd::LogicError("$propertyName($propertyName) does not have a type.  ");}
                    if (is_null($reflectionProperty->getType())) {throw ErrorFromCfd::LogicError("3reflectionProperty is not there ");}

                    #assert($reflectionProperty->getType(),"ouch");
                    #assert(!is_null($reflectionProperty->getType()), "ouch: ");
                    $typeIsAsString = $reflectionProperty->getType()->getName(); //https://www.php.net/manual/en/reflectiontype.tostring.php
                    $isNullable = $isNullAnAllowedType = $reflectionProperty->getType()->allowsNull();


                    $arrImplements = is_object($typeIsAsString) ? class_implements( $typeIsAsString) : [$typeIsAsString];
                    $doesImplementDeStringable = key_exists('\SchoolTwist\Cfd\Core\DeStringableInterface', $arrImplements);
                }
                $richProperties[$propertyName]['mustBeInitialized'] = $mustBeInitialized ? 1 : 0;
                $richProperties[$propertyName]['type'] = $typeIsAsString;
                $richProperties[$propertyName]['isNullAnAllowedType'] = $isNullable ? 1 : 0;


                /* 1) Handle builtin name mismatches.
                     public int $i; // this returns a type with name 'integer' which is annoying
                     public bool $b;// this returns a type with name 'boolean'.
                     We're going to account for this by saying it can be of type 'int' or 'integer'.
                     I suspect this will problematic in the future, but I'm not sure.
                2) In php 8, we'll be able to let a property be restricted a list of specified types, like int, double..
                     This is supposed to handle that.  It used to work when in docblock days, but not really used.
                */

                if ($typeIsAsString == 'int') {
                    $richProperties[$propertyName]['types'] = [
                        $typeIsAsString,
                        'integer'
                    ];// in php 8, we should start allowing multiple types
                } elseif ($typeIsAsString == 'bool') {
                    $richProperties[$propertyName]['types'] = [
                        $typeIsAsString,
                        'boolean'
                    ];// in php 8, we should start allowing multiple types
                } elseif ($typeIsAsString == 'union - make better in php 8') {
                    $richProperties[$propertyName]['types'] =$arrTypes_withoutBrackets;// in php 8, we should start allowing multiple types
                } else {
                    $richProperties[$propertyName]['types'] = [$typeIsAsString];// in php 8, we should start allowing multiple types
                }
                if ($isNullable) {
                    $richProperties[$propertyName]['types'][] = 'null';
                }


                // Ensure no forbidden types
                $forbiddenTypes = $CfdClassName::FORBIDDEN_TYPES;
                $forbiddenTypesFoundHere = array_intersect($forbiddenTypes, $richProperties[$propertyName]['types']);
                if (count($forbiddenTypesFoundHere) > 0) {
                    $csvTypes = implode(', ', $forbiddenTypesFoundHere);
                    throw ErrorFromCfd::LogicError(
                        "This " . get_called_class(
                        ) . "($propertyName) is specked to be of type $csvTypes, but that is on the forbidden list."
                    );
                }
            }

            // is Required? Part Duex. 12/14/20'
            if (!$isTypeEnforced) {
                $richProperties[$propertyName]['isRequired'] = false;

            } else {
                $richProperties[$propertyName]['isRequired'] = !$hasDefault;
            }
            $richProperties[$propertyName]['doesImplementDeStringable'] = $doesImplementDeStringable;




              #\ClsProbe::singleton()->stop( __METHOD__."/foreach/$CfdClassName/$propertyName",$utcf);
        #\ClsProbe::singleton()->stop(__METHOD__.'/foreach',$utf);
        }


        $asr = ['_meta' =>
            ['numNonMeta' => $numNonMeta,
                'className'=>get_called_class()],
            'properties' => $richProperties];
        static::$_cachedRichProperties[$CfdClassName] = $asr;
        #\ClsProbe::singleton()->stop(__METHOD__.'/'.$CfdClassName,$utc);
        #\ClsProbe::singleton()->stop(__METHOD__,$ut);
        return $asr;

    }

    /* return all properties, even those non-tracked properties (but they at least need to be defined - nothing dynamic)
    // static vars are not exported
    // likeCl
    // class hw {
    // static $version; // not exported cuz static
    //  @var integer
    // public $age  // 'age' is expecte
    //
    //  public $height; // is export
    }
    $h = new hw();
    $h->eyes = 'hazel';
    // eyes are NOT exported, cuz not defined.

    //In most cases, this would be suitable for round-tripping
    */

    public function toDeepArray(): array
    {

        $asrRichPropreties = static::getRichProperties();
        $asrCfdProperties = [];
        foreach ($asrRichPropreties['properties'] as $propertyName=>$asrRichProperty) {
            if (!$asrRichProperty['isStatic']) {
                $asrCfdProperties[$propertyName] = $this->$propertyName;
            }
        }
        return $asrCfdProperties;

    }

    /* like toArray, but the tree is collapses so

    EtFramework19\Models\Team\CfdbTblBooker Object
    (
        [Email] => SchoolTwistWip\Cfd\Cfdb\Library\CfdbEmail Object
            (
                [Value] => tmp_test_1580908102_5e3abe4633299_wm@rohrer.org
            )

        [Phone] => SchoolTwistWip\Cfd\Cfdb\Library\CfdbPhone Object
            (
                [Value] => 61739806012
            )

        [User_Id] => EtFramework19\Models\Team\CfdbForeignKey_toUser Object
            (
                [Value] => 2
            )

        [Team_Uuid] => EtFramework19\Models\Team\CfdbForeignKey_toTeam Object
            (
                [Value] => b1be3874-4818-11ea-80bf-acde48001122
            )

        [Address_ofBooker_Uuid] =>
        [_OriginStory] => SchoolTwistWip\Cfd\Cfdb\Library\CfdbShortText Object
            (
                [Value] =>
            )

        [CreateOn] => SchoolTwistWip\Cfd\Cfdb\Library\CfdbDateTime Object
            (
                [Value] => 2020-02-05 08:09:08
            )

        [pk] => b1be8f72-4818-11ea-9ff1-acde48001122
        [FirstName] => SchoolTwistWip\Cfd\Cfdb\Library\CfdbShortString Object
            (
                [Value] => wm99a
            )

        [LastName] => SchoolTwistWip\Cfd\Cfdb\Library\CfdbShortString Object
            (
                [Value] => Rohrer
            )

    )

    --becomes--
    Array
        (
            [Email] => tmp_test_1580908102_5e3abe4633299_wm@rohrer.org
            [Phone] => 61739806012
            [User_Id] => 2
            [Team_Uuid] => b1be3874-4818-11ea-80bf-acde48001122
            [Address_ofBooker_Uuid] =>
            [_OriginStory] =>
            [CreateOn] => 2020-02-05 08:09:08
            [pk] => b1be8f72-4818-11ea-9ff1-acde48001122
            [FirstName] => wm99a
            [LastName] => Rohrer
        )
    */
    public function toShallowArray() : array
    {
        $asrDeepArray = $this->toDeepArray();
        foreach ($asrDeepArray as $propertyName=>$valueOrObject) {
            if (gettype($valueOrObject) == 'object') {
                if (property_exists($valueOrObject,'Value')) {
                    $valueOrObject = $Value = $valueOrObject->Value;
                    $asrDeepArray[$propertyName] = $valueOrObject;
                }

            }
        }
        return $asrDeepArray;
    }


    /* returns an array of property names that are under CFD control.  This will not return non-tracked properties.
    */
    public static function toArrayKeys_tracked(): array
    {
        $asrRichPropreties = static::getRichProperties();
        $arrTrackedNames = [];

        foreach ($asrRichPropreties['properties'] as $propertyName=>$asrRichProperty) {
            if ($asrRichProperty['isTypeEnforced']) {
                $arrTrackedNames[] = $propertyName;
            }
        }
        return $arrTrackedNames;
    }

}


abstract class DtoCfd_Base_DbRow extends CfdBase
{
    //abstract $Uuid; // we'd do this if the language allowed it.
}





