<?php
namespace SchoolTwist\Cfd\Core;

interface DeStringableInterface {
    public function from_string(string $valueAsString, bool $assumeValidValue);
}