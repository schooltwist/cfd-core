<?php
namespace SchoolTwist\Cfd\Lib;

class CfdYmd extends \SchoolTwist\Cfd\Core\CfdBase {
    /** @var string */
    public string $Value;

    public static function Value_Validates($maybeValidValue) : \SchoolTwist\Validations\Returns\DtoValid {
        $t = date("Y-m-d",strtotime($maybeValidValue)); // https://xkcd.com/1179/

        if ($maybeValidValue == $t) {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => true]);
        } else {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'enumReason'=>'NotRoundtripping','message'=>"$t !=$maybeValidValue" ]);
        }
    }
//    public static function upConvertType_elseNull(string $propertyName, $dangerousValue)
//    {
//        $incomingType = gettype($dangerousValue);
//        if ($incomingType == 'string') {
//            try {
//                $descendantName = get_called_class();
//                $dangerousValue_new = new $descendantName(['Value' => $dangerousValue]);
//            } catch (\Throwable $e) {
//                return null;
//            }
//            return $dangerousValue_new;
//
//        } else {
//            return null;
//        }
//    }
}

