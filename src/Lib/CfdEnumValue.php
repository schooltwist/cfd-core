<?php

namespace SchoolTwist\Cfd\Lib;


abstract class CfdEnumValue extends \SchoolTwist\Cfd\Core\CfdBase {
    /** @var string */
    public string $EnumValue;

    use property_EnumValue_Validates;
}

