<?php
namespace SchoolTwist\Cfd\Lib;


class CfdDateTime extends \SchoolTwist\Cfd\Core\CfdBase {
    /** @var string */
    public string $Value;

    public static function Value_Validates($maybeValidValue) : \SchoolTwist\Validations\Returns\DtoValid {
        $format = 'Y-m-d H:i:s';
//        return false;

        // Handle when they don't put anything after the day. 2000-11-04 would otherwise work

        $t = date($format,strtotime($maybeValidValue));
//        print "<br>time $maybeValidValue => $t";
//        exit;

        if ($maybeValidValue == $t) {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => true]);
        } else {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'enumReason'=>'NotRoundtripping','message'=>"$t !=$maybeValidValue Please pass data as exaclty $format " ]);
        }
    }

    public static function now_asString() : string
    {
        return date('Y-m-d H:i:s');//1970-11-04 13:11:25
    }


}




