<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

#require_once(__DIR__ . '/../../../vendor/autoload.php');

class DtoCfdInt extends \SchoolTwist\Cfd\Core\CfdBase {
    /** @var int */
    public $val;
}

final class TestDtoCfd_int extends TestCase
{
     function testPreValidationsSubmission_byProperty()
      {
          // print "One is a ". gettype(1); // prints 'integer'
          try {
              $DtoValid = DtoCfdInt::preValidateProperty('val', 1, null);
              $this->assertTrue( false, "This should fail by 'int' being on the forbidden list. " . get_called_class() . " DtoValid({$DtoValid->enumReason}). " . __LINE__);
          } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
              $this->assertTrue(true, "This should have failed hard (and gotten here) cuz int != integer, and php numbers are integers.  int is slang for integer.: " . get_called_class() . "  " . __LINE__);

          }

      }
}

