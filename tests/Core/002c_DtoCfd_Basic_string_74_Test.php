<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
class Simple00 extends \SchoolTwist\Cfd\Core\CfdBase
{
}

class Simple0yCfd extends \SchoolTwist\Cfd\Core\CfdBase
{
    public $caresGiven; // Not ok since in CFD, all public properties MUST be typed.
}

class Simple_0z_Cfd extends \SchoolTwist\Cfd\Core\CfdBase
{
    public ?int $caresGiven = null; // Just proving this compiles
}

class Simple_0_Cfd extends \SchoolTwist\Cfd\Core\CfdBase
{
    public int $caresGiven = 0; // must only create w/ an optional int
}

class Simple_0b_Cfd extends \SchoolTwist\Cfd\Core\CfdBase
{
    public ?int $caresGiven = 0; // must only create w/ an optional int, or null
}

class Simple_0c_Cfd extends \SchoolTwist\Cfd\Core\CfdBase
{
    public int $caresGiven; // must create with a mandatory int
}

class Simple_0d_Cfd extends \SchoolTwist\Cfd\Core\CfdBase
{
    public ?int $caresGiven; // must create with a mandatory int, or null
}

class Simple_0e_Cfd extends \SchoolTwist\Cfd\Core\CfdBase
{
    #public string $caresGiven2 = 0; // wouldn't event compile cuz type mismatch
}

class Simple_1_Cfd extends \SchoolTwist\Cfd\Core\CfdBase
{
    public string $name; // simple var, but it had better be a string when assigning
}

/**
 * Class CfdBase
 * @package SchoolTwist\Cfd\Core
 *
 *  // Since extending CfdBase, public properties all validated at creation
 *  // Any public property w/o a default is required to be set at creation
 *  // Public properties can be read, but not updated. To update, create a new copy with different inputs
 */
class NamedThingsCfd2 extends \SchoolTwist\Cfd\Core\CfdBase
{
    # protected const  META_PREFIXES = ['doesHave'];  //
    #  public CONST ALLOW_UPCONVERT_TYPES = ['integer','string'];
    public string $lastName; // simple var, but it had better be a string when assigning

    public int $numEyes; // simple var, but it had better be a int when assigning
    public ?int $age; // simple var, but it had better be a int when assigning. It can be null if we don't know the age
    // It must be assigned something

    public string $namesIKnow;  // There is a function

    // Optional Custom Validation
    public static function namesIKnow_Validates($untrustedValue): \SchoolTwist\Validations\Returns\DtoValid
    {
        if (in_array($untrustedValue, ['Tom', 'Bill', 'Chad', 'Lisa', 'Wendy'])) {
            return new DtoValid(
                [
                    'isValid' => true,
                    'enumReason' => 'Found',
                    'message' => "I know a person named $untrustedValue."
                ]
            );
        } else {
            return new DtoValid(
                [
                    'isValid' => false,
                    'enumReason' => 'NotFound',
                    'message' => "name($untrustedValue) is not a name of somebody I went to school with."
                ]
            );
        }
    }
}
class C {
    public function __construct(array $a) {
        1/0;
    }
}

class TestDtoCfd_BaseString2 extends TestCase
{
    function test_HelloWorld() {

        $this->assertEquals(1,1);
        $a = 1;
        $this->assertEquals(1,$a);
        $a = 2;
        $this->assertNotEquals(1,$a);
    }

    function test_Empty() {
        $c = new Simple00([]);
        $this->assertIsObject($c, "Nothing " . get_called_class() . "  " . __LINE__);
    }

    function test_RequireType()
    {
        try {
            $c = new Simple0yCfd([]);

            $this->assertTrue(false, "Should not get here: Not passed constructor array");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "This should have failed hard (and gotten here) cuz int != integer, and php numbers are integers.  int is slang for integer.: " . get_called_class() . "  " . __LINE__);
        }
    }


    function testItemWithoutTypedPropertiesProboblyNonsenseCfd()
    {

        try {
            $c = new Simple_0_Cfd();
            $this->assertTrue(false, "Should not get here: Not passed constructor array");
        } catch (ArgumentCountError $e) {
            $this->assertTrue(true, "This should have failed hard (and gotten here) cuz int != integer, and php numbers are integers.  int is slang for integer.: " . get_called_class() . "  " . __LINE__);
        }


        $c = new Simple_0_Cfd([]);
        $this->assertTrue($c->caresGiven == 0, "ok, cuz careGiven has a default");

        $c = new Simple_0_Cfd(['caresGiven' => 1]);
        $this->assertTrue($c->caresGiven == 1, "ok, I assigned a number. This also tests that integer converts to int, which is a 7.4 thingy");

        try {
            $c = new Simple_0_Cfd(['caresGiven' => 1.1]);
            $this->assertTrue(false, "Should not get here: cuz 1.1 is a float, not an int");
        } catch (SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "This should have failed hard (and gotten here) cuz int != integer, and php numbers are integers.  int is slang for integer.: " . get_called_class() . "  " . __LINE__);
        }


    }

//    function testItemWithoutTypedPropertiesProboblyNonsenseCfd2()
//    {
//          try {
//              $c = new Simple_0b_Cfd([]);
//              $this->assertTrue( false, "Should not get here: This one requires initial value cuz no default");
//          } catch (ArgumentCountError $e) {
//              $this->assertTrue(true, "This should have failed hard (and gotten here): " . get_called_class() . "  " . __LINE__);
//          }
//
//    }
//     function testHaveSomethingSet()
//      {
//
//          try {
//              $c = new String2Cfd([]);
//              $this->assertTrue( false, "This should fail cuz no typed properties ");
//          } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
//              $this->assertTrue(true, "This should have failed hard (and gotten here) cuz int != integer, and php numbers are integers.  int is slang for integer.: " . get_called_class() . "  " . __LINE__);
//
//          }
//
//          try {
//              $c = new String2Cfd([1]);
//              $this->assertTrue( false, "This should fail cuz not associative array ");
//          } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
//              $this->assertTrue(true, "This should have failed hard (and gotten here) cuz int != integer, and php numbers are integers.  int is slang for integer.: " . get_called_class() . "  " . __LINE__);
//
//          }
//
//
//      }
//
//
//    function testPreValidationsSubmission_byProperty()
//    {
//        $DtoValid = String2Cfd::preValidateProperty('val', 1, null);
//        $this->assertTrue($DtoValid->isValid == false,
//                          "Should not see this: " . get_called_class(
//                          ) . " DtoValid({$DtoValid->enumReason}). " . __LINE__
//        );
//
//        $DtoValid = String2Cfd::preValidateProperty('val', "1", null);
//        $this->assertTrue($DtoValid->isValid == true,
//                          "Should not see this: " . get_called_class(
//                          ) . " DtoValid({$DtoValid->enumReason}). " . __LINE__
//        );
//    }
//
//    function testPreValidationsSubmission_byProperty_null()
//    {
//        $DtoValid = String2Cfd::preValidateProperty('val', null, null);
//        $this->assertTrue($DtoValid->isValid == static::$isNullAnOption,
//                          "Should not see this: " . get_called_class(
//                          ) . " DtoValid({$DtoValid->enumReason}). " . __LINE__
//        );
//
//
//        // force it to be required
//        $DtoValid = String2Cfd::preValidateProperty('val', null, true);
//        $this->assertTrue($DtoValid->isValid == false,
//                          "Should not see this: " . get_called_class(
//                          ) . " DtoValid({$DtoValid->enumReason}). " . __LINE__
//        );
//    }
//
//
//    function testPreValidationsSubmission_easy()
//    {
//        $DtoValid = String2Cfd::preValidateSubmission(['val' => 1]);
//        $this->assertTrue($DtoValid->isValid == false,
//                          "Should not see this: " . get_called_class(
//                          ) . " DtoValid({$DtoValid->enumReason}). " . __LINE__
//        );
//
//        $DtoValid = String2Cfd::preValidateSubmission(['val' => "Hello"]);
//        $this->assertTrue($DtoValid->isValid == true,
//                          "Should not see this:  " . get_called_class(
//                          ) . " DtoValid({$DtoValid->enumReason}). " . __LINE__
//        );
//
//        $DtoValid = String2Cfd::preValidateSubmission(['val' => "1"]);
//        $this->assertTrue($DtoValid->isValid == true,
//                          "Should not see this:  " . get_called_class(
//                          ) . " DtoValid({$DtoValid->enumReason}). " . __LINE__
//        );
//
//        $DtoValid = String2Cfd::preValidateSubmission(['val' => ""]);
//        $this->assertTrue($DtoValid->isValid == true,
//                          "Should not see this:  " . get_called_class(
//                          ) . " DtoValid({$DtoValid->enumReason}). " . __LINE__
//        );
//    }
//
//    function testPreValidationsSubmission_null()
//    {
//        $DtoValid = String2Cfd::preValidateSubmission(['val' => null]);
//        $this->assertTrue($DtoValid->isValid == static::$isNullAnOption,
//                          "Should not see this:  " . get_called_class(
//                          ) . " DtoValid({$DtoValid->enumReason}). " . __LINE__
//        );
//    }
}
