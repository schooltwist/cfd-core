<?php

declare(strict_types=1);

namespace testworld510;

use PHPUnit\Framework\TestCase;


class ATblMaybe extends \SchoolTwist\Cfd\Core\CfdBase
{
    public int $pk;
}

class ATblMaybeComplex extends \SchoolTwist\Cfd\Core\CfdBase
{
    /** @var mixed*/
    public $pk;
}


final class Test_515_upConvertFrombaseTypes_ensureVeryDeepWorks_Test extends TestCase
{


    function testHW()
    {

        // base
        $dtoValid = ATblMaybe::preValidateProperty('pk', 1, null);
        $this->assertTrue($dtoValid->isValid == true, '');


        // composite
        $c = new ATblMaybe(['pk'=>11]);
        $this->assertTrue(true, '');
    }

    function testStringToInt()
    {
        // base
        $dtoValid = ATblMaybe::preValidateProperty('pk', '1', null);
        $this->assertEquals(true, $dtoValid->isValid , '');

        $dtoValid = ATblMaybe::preValidateProperty('pk', '1.1', null);
        $this->assertEquals(false, $dtoValid->isValid , '');

        #$this->assertEquals( 'string2int',$dtoValid->enumReason, '');

        // composite
        $c = new ATblMaybe(['pk'=>'4']);
        $this->assertTrue(true, '');
    }

    function testStringToMulitType()
    {

        // base
        $dtoValid = ATblMaybeComplex::preValidateProperty('pk', 1, null);
        $this->assertTrue($dtoValid->isValid == true, '');


        // composite
        $c = new ATblMaybeComplex(['pk'=>11]);
        $this->assertTrue(true, '');


        // base
        $dtoValid = ATblMaybeComplex::preValidateProperty('pk', '1', null);
        $this->assertEquals(true, $dtoValid->isValid , '');

        $dtoValid = ATblMaybeComplex::preValidateProperty('pk', '1.1', null);
        $this->assertEquals(true, $dtoValid->isValid , '');

        #$this->assertEquals( 'string2int',$dtoValid->enumReason, '');

        // composite
        $c = new ATblMaybeComplex(['pk'=>'4']);
        $this->assertTrue(true, '');
    }



//     function testBad()
//    {
//         $asrData = [
//             'ConvertableLuck' => 89,
//        ];
//        $dtoValid = Profile2::preValidateSubmission($asrData);
//        $this->assertTrue(!$dtoValid->isValid, '');
//        $this->assertTrue(isset($dtoValid->enumReason), '');
//        $this->assertTrue($dtoValid->enumReason == 'unlucky', " dtoValid->enumReason({$dtoValid->enumReason})" );
//        print_r($dtoValid);
//        exit;
//
//        $asrData['ConvertableLuck'] = 88;
//        $dtoValid = Profile2::preValidateSubmission($asrData);
//        $this->assertTrue($dtoValid->isValid, '');
//
//        $asrData['ConvertableLuck'] = 1;
//        $dtoValid = Profile2::preValidateSubmission($asrData);
//        $this->assertTrue($dtoValid->isValid, 'someting');
//
//    }


}
