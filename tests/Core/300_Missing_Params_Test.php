<?php

declare(strict_types=1);

namespace testworld;

use PHPUnit\Framework\TestCase;


class FunnyNumbers2 extends \SchoolTwist\Cfd\Core\CfdBase
{


    public int $LuckyNum;


    public int $Age;
}




final class Test_ObjParams32 extends TestCase
{
    function testHW()
    {

        $asrData = [
            'LuckyNum' => 5,
            'Age' => 48
        ];
        $obj = new FunnyNumbers2($asrData);
        $this->assertTrue($obj->LuckyNum == 5, '');
    }

     function testUnwantedNull()
    {
        $asrData = [
            'LuckyNum' => null,
            'Age' => 48
        ];
        try {
            $obj = new FunnyNumbers2($asrData);
            $this->assertTrue(false, 'shouldnot get here');
        } catch (\Throwable $e) {
            $this->assertTrue(true, 'ok');
        }
    }

    function testMissing()
    {

        $asrData = [
            //'LuckyNum' => 5,
            'Age' => 48
        ];
        try {
            $obj = new FunnyNumbers2($asrData);
            $this->assertTrue(false, 'shouldnot get here');
        } catch (\Throwable $e) {
            $this->assertTrue(true, 'ok');
        }
    }
}
