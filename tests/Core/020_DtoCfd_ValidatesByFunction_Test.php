<?php
declare(strict_types=1);
namespace testworld;


use PHPUnit\Framework\TestCase;

class DtoDummy extends \SchoolTwist\Cfd\Core\CfdBase {

    public int $Even;

    public static function Even_Validates($maybeValidValue) : \SchoolTwist\Validations\Returns\DtoValid {
        $isEven = ($maybeValidValue % 2) == 0;
        if ($isEven) {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => true]);
        } else {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'enumReason'=>'HasRemainder']);
        }
    }
}


final class TestDtoCfdw extends TestCase {


    function testBasics() {
        $obj = new \testworld\DtoDummy(['Even'=>0]);
        $this->assertTrue($obj->Even == 0, "Good");

        try {
            $obj = new \testworld\DtoDummy(['Even'=>1]);
            $this->assertTrue(0, "Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

        $obj = new \testworld\DtoDummy(['Even'=>2]);
        $this->assertTrue($obj->Even == 2, "Good");

        $obj = new \testworld\DtoDummy(['Even'=>-4]);
        $this->assertTrue($obj->Even == -4, "Good");

        try {
            $obj = new \testworld\DtoDummy(['Even'=>-3]);
            $this->assertTrue(0, "Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }


        try {
            $obj = new \testworld\DtoDummy(['Even'=>-1]);
            $this->assertTrue(0, "Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

        try {
            $obj = new \testworld\DtoDummy(['Even'=>3]);
            $this->assertTrue(0, "Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

    }


}