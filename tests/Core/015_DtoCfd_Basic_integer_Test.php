<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;


#require_once(__DIR__ . '/../../../vendor/autoload.php');


class DtoCfdInteger extends \SchoolTwist\Cfd\Core\CfdBase
{

    public integer $val;
}

class DtoCfdInteger_optional extends \SchoolTwist\Cfd\Core\CfdBase
{

    public ?integer $val;
}

//class DtoCfdInteger_optionalReversedOrder extends \SchoolTwist\Cfd\Core\CfdBase
//{
//    /** @var null|integer */
//    public integer $val;
//}


abstract class TestDtoCfd_BaseInt extends TestCase
{
    static $CfdName = 'TBD';
    static $ValName = 'TBD';
    static $isNullAnOption = 'TBD';

    function testPreValidationsSubmission_byProperty()
    {
        $DtoValid = static::$CfdName::preValidateProperty(static::$ValName, 1, null);
        $this->assertTrue($DtoValid->isValid == true, "Should not see this: " . get_called_class() . " DtoValid({$DtoValid->message}). " . __LINE__);

        $DtoValid = static::$CfdName::preValidateProperty(static::$ValName, "1", null);
        $this->assertTrue($DtoValid->isValid == false, "Should not see this: " . get_called_class() . " DtoValid({$DtoValid->message}). " . __LINE__);

    }

    function testPreValidationsSubmission_byProperty_null()
    {
        $DtoValid = static::$CfdName::preValidateProperty(static::$ValName, null, null);
        $this->assertTrue($DtoValid->isValid == static::$isNullAnOption, "Should not see this: " . get_called_class() . " DtoValid({$DtoValid->message}). " . __LINE__);
    }


    function testPreValidationsSubmission_easy()
    {
        $DtoValid = static::$CfdName::preValidateSubmission([static::$ValName => "Hello"]);
        $this->assertTrue($DtoValid->isValid == false, "Should not see this:  " . get_called_class() . " DtoValid({$DtoValid->message}). " . __LINE__);

        $DtoValid = static::$CfdName::preValidateSubmission([static::$ValName => "1"]);
        $this->assertTrue($DtoValid->isValid == false, "Should not see this:  " . get_called_class() . " DtoValid({$DtoValid->message}). " . __LINE__);

        $DtoValid = static::$CfdName::preValidateSubmission([static::$ValName => ""]);
        $this->assertTrue($DtoValid->isValid == false, "Should not see this:  " . get_called_class() . " DtoValid({$DtoValid->message}). " . __LINE__);

    }

    function testPreValidationsSubmission_allowed()
    {
        $DtoValid = static::$CfdName::preValidateSubmission([static::$ValName => 1]);
        $this->assertTrue($DtoValid->isValid == true, "Should not see this: " . get_called_class() . " DtoValid({$DtoValid->message}). " . __LINE__);

        $DtoValid = static::$CfdName::preValidateSubmission([static::$ValName => 99]);
        $this->assertTrue($DtoValid->isValid == true, "Should not see this: " . get_called_class() . " DtoValid({$DtoValid->message}). " . __LINE__);

        $DtoValid = static::$CfdName::preValidateSubmission([static::$ValName => -1]);
        $this->assertTrue($DtoValid->isValid == true, "Should not see this: " . get_called_class() . " DtoValid({$DtoValid->message}). " . __LINE__);

        $DtoValid = static::$CfdName::preValidateSubmission([static::$ValName => 0]);
        $this->assertTrue($DtoValid->isValid == true, "Should not see this: " . get_called_class() . " DtoValid({$DtoValid->message}). " . __LINE__);
    }


    function testPreValidationsSubmission_null()
    {
        $DtoValid = static::$CfdName::preValidateSubmission([static::$ValName => null]);
        $this->assertTrue($DtoValid->isValid == static::$isNullAnOption, "Should not see this:  " . get_called_class() . " DtoValid({$DtoValid->message}). " . __LINE__);
    }
}

final class TestDtoCfd_Submission_OneVal15 extends TestDtoCfd_BaseInt
{
    static $CfdName = 'DtoCfdInteger';
    static $ValName = 'val';
    static $isNullAnOption = false;
}

final class TestDtoCfd_Submission_OneVal_orNull15 extends TestDtoCfd_BaseInt
{
    static $CfdName = 'DtoCfdInteger_optional';
    static $ValName = 'val';
    static $isNullAnOption = true;
}

//final class TestDtoCfd_Submission_OneVal_orNullReordered15 extends TestDtoCfd_BaseInt
//{
//    static $CfdName = 'DtoCfdInteger_optionalReversedOrder';
//    static $ValName = 'val';
//    static $isNullAnOption = true;
//}
