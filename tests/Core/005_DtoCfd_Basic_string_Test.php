<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;




class DtoCfdString extends \SchoolTwist\Cfd\Core\CfdBase {
    public string $val;
}

class DtoCfdString_optional extends \SchoolTwist\Cfd\Core\CfdBase {

    public ?string $val;
}

class DtoCfdString_optionalOrder extends \SchoolTwist\Cfd\Core\CfdBase {

    public ?string $val;
}



abstract class TestDtoCfd_BaseString extends TestCase {
     static $CfdName = 'TBD';
     static $ValName = 'TBD';
     static $isNullAnOption = 'TBD';

      function testPreValidationsSubmission_byProperty()
      {
          $DtoValid = static::$CfdName::preValidateProperty(static::$ValName, 1, null);
            $this->assertTrue($DtoValid->isValid == false, "Should not see this: ".get_called_class()." DtoValid({$DtoValid->enumReason}-->{$DtoValid->message}). " . __LINE__);

            $DtoValid = static::$CfdName::preValidateProperty(static::$ValName, "1", null);
            $this->assertTrue($DtoValid->isValid == true, "Should not see this: ".get_called_class()." DtoValid({$DtoValid->enumReason}-->{$DtoValid->message}). " . __LINE__);

      }
      function testPreValidationsSubmission_byProperty_null()
      {
            $DtoValid = static::$CfdName::preValidateProperty(static::$ValName, null, null);
            $this->assertTrue($DtoValid->isValid == static::$isNullAnOption, "Should not see this: ".get_called_class()." DtoValid({$DtoValid->enumReason}-->{$DtoValid->message}). " . __LINE__);


            // force it to be required
            $DtoValid = static::$CfdName::preValidateProperty(static::$ValName, null, true);
            $this->assertTrue($DtoValid->isValid == false, "Should not see this: ".get_called_class()." DtoValid({$DtoValid->enumReason}-->{$DtoValid->message}). " . __LINE__);
      }


     function testPreValidationsSubmission_easy() {
        $DtoValid = static::$CfdName::preValidateSubmission([static::$ValName => 1]);
        $this->assertTrue($DtoValid->isValid == false, "Should not see this: ".get_called_class()." DtoValid({$DtoValid->enumReason}-->{$DtoValid->message}). " . __LINE__);

        $DtoValid =  static::$CfdName::preValidateSubmission([static::$ValName => "Hello"]);
        $this->assertTrue($DtoValid->isValid == true, "Should not see this:  ".get_called_class()." DtoValid({$DtoValid->enumReason}-->{$DtoValid->message}). " . __LINE__);

        $DtoValid =  static::$CfdName::preValidateSubmission([static::$ValName => "1"]);
        $this->assertTrue($DtoValid->isValid == true, "Should not see this:  ".get_called_class()." DtoValid({$DtoValid->enumReason}-->{$DtoValid->message}). " . __LINE__);

        $DtoValid =  static::$CfdName::preValidateSubmission([static::$ValName => ""]);
        $this->assertTrue($DtoValid->isValid == true, "Should not see this:  ".get_called_class()." DtoValid({$DtoValid->enumReason}-->{$DtoValid->message}). " . __LINE__);

    }

     function testPreValidationsSubmission_null()
     {
             $DtoValid = static::$CfdName::preValidateSubmission([static::$ValName => null]);
             $this->assertTrue($DtoValid->isValid == static::$isNullAnOption, "Should not see this:  " . get_called_class() . " DtoValid({$DtoValid->enumReason}-->{$DtoValid->message}). " . __LINE__);
     }
}

final class TestDtoCfd_Submission_OneVal5 extends TestDtoCfd_BaseString
{
    static $CfdName = 'DtoCfdString';
    static $ValName = 'val';
    static $isNullAnOption = false;
}
final class TestDtoCfd_Submission_OneVal_orNull5 extends TestDtoCfd_BaseString
{
    static $CfdName = 'DtoCfdString_optional';
    static $ValName = 'val';
    static $isNullAnOption = true;
}
final class TestDtoCfd_Submission_OneVal_orNullReordered5 extends TestDtoCfd_BaseString
{
    static $CfdName = 'DtoCfdString_optionalOrder';
    static $ValName = 'val';
    static $isNullAnOption = true;
}
