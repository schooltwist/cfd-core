<?php

declare(strict_types=1);

namespace testworld;

use PHPUnit\Framework\TestCase;

#use \EtElegantTechnologes\Pages;

#
#require_once(__DIR__ . '/../../../vendor/autoload.php');

class Greeting
{
    public function talk(?string $salution)
    {
        if ($salution) {
            return $salution;
        } else {
            return "Hello World";
        }
    }
}

class SubGreeting extends Greeting
{
}

class DtoDummyDum extends \SchoolTwist\Cfd\Core\CfdBase
{

    public \testworld\Greeting $myGreeting;


    public int $myAge;
}
class SmallPrimes extends \SchoolTwist\Cfd\Core\CfdBase
{

    public int $Value;
    public static function Value_Validates($val) : \SchoolTwist\Validations\Returns\DtoValid
    {
        return (in_array($val,[1,3,5,7])) ? (new \SchoolTwist\Validations\Returns\DtoValid(['isValid'=>true])) : (new \SchoolTwist\Validations\Returns\DtoValid(['isValid'=>false]));
    }
}

class FunnyNumbers extends \SchoolTwist\Cfd\Core\CfdBase
{


    public \testworld\SmallPrimes $SP;


    public int $Age;
}




final class Test_ObjParams extends TestCase
{
    function testMissingProperty_standardObjs()
    {
        $asrData = [
            'myAge'=>9,
            'myGreeting'=> new Greeting()
            ];
        $obj = new DtoDummyDum($asrData);
        $this->assertTrue($obj->myGreeting->talk(null) == 'Hello World','');

        $asrData = [
            'myAge'=>10,
            'myGreeting'=> new SubGreeting()
            ];
        $obj = new DtoDummyDum($asrData);
        $this->assertTrue($obj->myGreeting->talk(null) == 'Hello World','');
    }

         function testMissingProperty_other()
    {
        $asrData = [
            'SP'=>5,
            'Age'=> 49,
            ];
        try {
            $obj = new FunnyNumbers($asrData);
            $this->assertTrue(false, 'never');
        } catch (\Throwable $e) {
               $this->assertTrue(true, 'ok');
        }

    }

     function testMissingProperty_Cfds()
    {


         $cfdsp = new SmallPrimes(['Value'=>7]);


         $asrData = [
            'SP'=>$cfdsp,
            'Age'=> 49,
            ];
        $obj = new FunnyNumbers($asrData);
        $this->assertTrue($obj->Age == 49,'');
        $this->assertTrue($obj->SP->Value == 7,'');
    }
}