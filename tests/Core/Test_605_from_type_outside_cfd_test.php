<?php
declare(strict_types=1);

namespace testworld510;

use PHPUnit\Framework\TestCase;
use SchoolTwist\Cfd\Core\CfdBase;


class StandardClassWithoutTypeConversion // Notice: this is not a CFD
{
    public string $v;
}

class CfdClassWithReferenceToGenericClassThatCanNotConvert extends CfdBase {
    public StandardClassWithoutTypeConversion $s;
}

class StandardClassTypeConversion_NowWithConversion // Notice: this is not a CFD
{
    public string $v;
    public static function from_string(string $val, bool $assume_valid) : self {
        $s = new self();
        $s->v = $val;
        return $s;
    }
}

class CfdClassWithReferenceToGenericClass_NowConverts extends CfdBase {
    public StandardClassTypeConversion_NowWithConversion $s;
}

final class Test_515_upConvertFrombaseTypes_ensureVeryDeepWorks_Test extends TestCase
{


    function testHW()
    {

        // Verify not autoconverting
        $a = 1;
        $dtoValid = CfdClassWithReferenceToGenericClassThatCanNotConvert::preValidateProperty('s', $a, null);
        $this->assertFalse($dtoValid->isValid, '');

        // Verify not autoconverting
        $a = '1';
        $dtoValid = CfdClassWithReferenceToGenericClassThatCanNotConvert::preValidateProperty('s', $a, null);
        $this->assertFalse($dtoValid->isValid, '');


        // verify works when we get the right type
        $a = new StandardClassWithoutTypeConversion();
        $dtoValid = CfdClassWithReferenceToGenericClassThatCanNotConvert::preValidateProperty('s', $a, null);
        $this->assertTrue($dtoValid->isValid, '');

        $c = new CfdClassWithReferenceToGenericClassThatCanNotConvert(['s'=>$a]);
        $this->assertTrue(true, 'did not crash');


        // ------- Now, should convert when passed a string --------
        // Verify not autoconverting, cuz int (not string
        $a = 1;
        $dtoValid = CfdClassWithReferenceToGenericClass_NowConverts::preValidateProperty('s', $a, null);
        $this->assertFalse($dtoValid->isValid, '');

        // Verify autoconverting (cuz got a string)
        $a = '1';
        $dtoValid = CfdClassWithReferenceToGenericClass_NowConverts::preValidateProperty('s', $a, null);
        $this->assertTrue($dtoValid->isValid, '');

        $c = new CfdClassWithReferenceToGenericClass_NowConverts(['s'=>$a]);
        $this->assertTrue(true, 'did not crash');


        // verify still works when we get the right type
        $a = new StandardClassWithoutTypeConversion(); // wrong class
        $dtoValid = CfdClassWithReferenceToGenericClass_NowConverts::preValidateProperty('s', $a, null);
        $this->assertFalse($dtoValid->isValid, '');


        $a = new StandardClassTypeConversion_NowWithConversion();  // right class
        $dtoValid = CfdClassWithReferenceToGenericClass_NowConverts::preValidateProperty('s', $a, null);
        $this->assertTrue($dtoValid->isValid, '');





    }


}
