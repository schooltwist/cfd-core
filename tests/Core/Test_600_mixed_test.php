<?php
declare(strict_types=1);

namespace testworld510;

use phpDocumentor\Reflection\Types\Mixed_;
use PHPUnit\Framework\TestCase;
use SchoolTwist\Validations\Returns\DtoValid;


class Mixed extends \SchoolTwist\Cfd\Core\CfdBase
{
    /** @var mixed */
    public $untyped;
}

class MixedWithValidation extends \SchoolTwist\Cfd\Core\CfdBase
{
    /** @var mixed */
    public $untyped;

    public static function untyped_validates($dangerous): DtoValid {
        if (in_array($dangerous, ['JJ','Lisa',88])) {
            return new DtoValid(['isValid'=>true]);
        } else {
            return new DtoValid(['isValid'=>false]);
        }
    }
}



final class Test_515_upConvertFrombaseTypes_ensureVeryDeepWorks_Test extends TestCase
{


    function testHW()
    {

        // base
        $a = 11;
        $dtoValid = Mixed::preValidateProperty('untyped', $a, null);
        $this->assertTrue($dtoValid->isValid , '');
        $c = new Mixed(['untyped'=>$a]);
        $this->assertTrue(true, 'did not crash');

        $a = '11';
        $dtoValid = Mixed::preValidateProperty('untyped', $a, null);
        $this->assertTrue($dtoValid->isValid , '');
        $c = new Mixed(['untyped'=>$a]);
        $this->assertTrue(true, 'did not crash');

        $a = true;
        $dtoValid = Mixed::preValidateProperty('untyped', $a, null);
        $this->assertTrue($dtoValid->isValid , '');
        $c = new Mixed(['untyped'=>$a]);
        $this->assertTrue(true, 'did not crash');
    }

    function testHWWithValidation()
    {

        // base
        $a = 'JJ';
        $dtoValid = MixedWithValidation::preValidateProperty('untyped', $a, null);
        $this->assertTrue($dtoValid->isValid , '');
        $c = new MixedWithValidation(['untyped'=>$a]);
        $this->assertTrue(true, 'did not crash');

        $a = 'JJWontValidate';
        $dtoValid = MixedWithValidation::preValidateProperty('untyped', $a, null);
        $this->assertFalse($dtoValid->isValid , '');
        //  cuz didn't validate      $c = new MixedWithValidation(['untyped'=>$a]);
        //        $this->assertTrue(true, 'did not crash');

        $a = 'Lisa';
        $dtoValid = MixedWithValidation::preValidateProperty('untyped', $a, null);
        $this->assertTrue($dtoValid->isValid , '');
        $c = new MixedWithValidation(['untyped'=>$a]);
        $this->assertTrue(true, 'did not crash');

        $a = 88;
        $dtoValid = MixedWithValidation::preValidateProperty('untyped', $a, null);
        $this->assertTrue($dtoValid->isValid , '');
        $c = new MixedWithValidation(['untyped'=>$a]);
        $this->assertTrue(true, 'did not crash');
    }




}
