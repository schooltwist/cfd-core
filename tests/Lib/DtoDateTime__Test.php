<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;





final class TestDtoDateTime extends TestCase {


    function testMakeBad()
    {
        try {
            $badDbt = ';lakjsdf;';
            $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value' => $badDbt]);
            $this->assertTrue(0, "1Should not get this far ut: " . strtotime($badDbt));
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }

        try {
            $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value' => 0]);
            $this->assertTrue(0, "2Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "2Good - that faiiled as expected");
        }


        try {
            $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value' => 1970]);
            $this->assertTrue(0, "3Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "3Good - 1970 iis not a string, plus it is a vague date");
        }

        try {
            $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value' => 'tomorrow']);
            $this->assertTrue(0, "4Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "4Good - that faiiled as expected");
        }

        try {
            $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value' => '1970-11-04']);
            $this->assertTrue(0, "4Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "4Good - that faiiled as expected. Needs a time after it.");
        }
    }

    function test_makeGood() {

        $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value'=>'1970-11-04 13:11:25']);
        $this->assertTrue(isset($obj), "");

        $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value'=>\SchoolTwist\Cfd\Lib\CfdDateTime::now_asString()]);
        $this->assertTrue(isset($obj), "");

    }



}