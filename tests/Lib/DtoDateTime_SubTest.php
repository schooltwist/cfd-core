<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;


class CfdbDateTimeWrong  extends \SchoolTwist\Cfd\Lib\CfdDateTime {
}

class CfdbDateTime extends \SchoolTwist\Cfd\Lib\CfdDateTime
{
    public static function getSqlCreateColumn(): \EtFramework19\Cfdb\Core\CfdSqlCreateCol
    {
        return new \EtFramework19\Cfdb\Core\CfdSqlCreateCol([
            'OriginalPropertyName' => 'Value',
            'SqlType' => "datetime",
            #'Comment'=>,
            'IsUnique' => false,
            'canBeNull' => true,
            'hasDefault' => true,
            'Default' => NULL,
        ]);
    }
    public static function dbStringCastBack($stringStraigtFromDb)
    {
        return $stringStraigtFromDb;
    }
}

class DtoDbWip_Basketlaskdjflasdkfj extends \SchoolTwist\Cfd\Core\CfdBase {
    /* CREATE TABLE `wp_etac_events_Basket` (
      `Uuid` varchar(255) NOT NULL,
      `_OriginStory` text COMMENT 'The csvBookingIds probably regarding how this basket was derived.',
      `EnumPhase` varchar(255) DEFAULT NULL COMMENT 'Draft, OnOrbit, Trash, Museum',
      `BornOnDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      `_ReplacedByOtherUuid` varchar(255) DEFAULT NULL COMMENT 'Generally merged into new Uuid. Regardless, link by user to this Basket will redirect to this other Basket. ',
      PRIMARY KEY (`Uuid`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8
    */


    public string $Uuid;

    public \SchoolTwist\Cfd\Lib\CfdDateTime $DtoDateTime_BornOn;

    #public $DtoEnumValue_BasketPhase; // untracked 8/20 'untracked isn't a thing
}



final class TestDtoDateTime2 extends TestCase {


    function testMakeBad()
    {
        try {
            $badDbt = ';lakjsdf;';
            $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value' => $badDbt]);
            $this->assertTrue(0, "1Should not get this far ut: " . strtotime($badDbt));
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }

        try {
            $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value' => 0]);
            $this->assertTrue(0, "2Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "2Good - that faiiled as expected");
        }


        try {
            $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value' => 1970]);
            $this->assertTrue(0, "3Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "3Good - 1970 iis not a string, plus it is a vague date");
        }

        try {
            $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value' => 'tomorrow']);
            $this->assertTrue(0, "4Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "4Good - that faiiled as expected");
        }

        try {
            $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value' => '1970-11-04']);
            $this->assertTrue(0, "4Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "4Good - that faiiled as expected. Needs a time after it.");
        }
    }

    function test_makeGood() {

        $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value'=>'1970-11-04 13:11:25']);
        $this->assertTrue(isset($obj), "");

        $obj = new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value'=>\SchoolTwist\Cfd\Lib\CfdDateTime::now_asString()]);
        $this->assertTrue(isset($obj), "");

    }

     function test_inheritenceCuzHadAnIssue() {

        $obj = new CfdbDateTime(['Value'=>'1970-11-04 13:11:25']);
        $this->assertTrue(isset($obj), "");

        $obj = new CfdbDateTime(['Value'=>\SchoolTwist\Cfd\Lib\CfdDateTime::now_asString()]);
        $this->assertTrue(isset($obj), "");
    }

     function test_inheritenceCuzHadAnIssue2() {

        // be bad

         try {
             $DtoBasket = new DtoDbWip_Basketlaskdjflasdkfj([
                 'Uuid' => 'hi im uuid',
                 'DtoEnumValue_BasketPhase' => 'untracked, so what evs',
                 'DtoDateTime_BornOn' => new CfdbDateTimeWrong(['Value' => '1970-11-04 13:11:25']),
             ]);
             $this->assertTrue(false, "never this far");
         } catch (Throwable $e) {
             $this->assertTrue(true, "");
         }



        //         // works cuz forces to base type
        //         $DtoBasket = new DtoDbWip_Basketlaskdjflasdkfj;([
        //             'Uuid' => 'hi im uuid',
        //             'DtoEnumValue_BasketPhase' => 'untracked, so what evs',
        //             'DtoDateTime_BornOn' => new \SchoolTwist\Cfd\Lib\CfdDateTime(['Value' => '1970-11-04 13:11:25']),
        //         ]);
        //
        //         $this->assertTrue(true, "never this far");


         // should work cuz decendant of base type
        $DtoBasket = new DtoDbWip_Basketlaskdjflasdkfj([
             'Uuid' => 'hi im uuid',
             # 8/20' this used to be ok, but now ALL public properties must be typed. 'DtoEnumValue_BasketPhase' => 'untracked, so what evs',
             'DtoDateTime_BornOn' => new CfdbDateTime(['Value' => '1970-11-04 13:11:25']),
         ]);
         $this->assertTrue(true, "never this far");


    }
}
