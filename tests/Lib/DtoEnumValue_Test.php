<?php
declare(strict_types=1);
namespace testworld;




use PHPUnit\Framework\TestCase;

#require_once(__DIR__ . '/../../../vendor/autoload.php');
class DtoCfdInt extends \SchoolTwist\Cfd\Core\CfdBase {
    public int $val;
}

class CfdEnumPhaseSimple extends \SchoolTwist\Cfd\Lib\CfdEnumValue {
    public string $EnumValue;
    public static array $_ArrEnumValuePossibilities = ['Draft', 'RollOut', 'LaunchPad', 'OnOrbit', 'Descent', 'Museum', 'Trash'];

}

class CfdEnumPhase extends \SchoolTwist\Cfd\Lib\CfdEnumValue {
    public string $EnumValue;
    public static $bob = 'hi';
    public static array $_ArrEnumValuePossibilities = ['Draft','RollOut','LaunchPad', 'OnOrbit', 'Descent', 'Museum', 'Trash'];
}

class CfdEnumSmallPrimes extends \SchoolTwist\Cfd\Lib\CfdEnumValue {
    public string $EnumValue;
    public static array $_ArrEnumValuePossibilities = [1,2,3,5,7];
}


final class TestDtoEnumPhase1 extends TestCase {

    function testBasics() {
        $obj = new CfdEnumPhaseSimple(['EnumValue' => 'Draft']);
        $this->assertTrue($obj->EnumValue == 'Draft', "Good");

    }

        function testBasics2() {
        $obj = new CfdEnumPhase(['EnumValue' => 'Draft']);
        $this->assertTrue($obj->EnumValue == 'Draft', "Good");


        try {
            $obj = new \testworld\CfdEnumPhase(['EnumValue' => 'Explosion']);
            $this->assertTrue(0, "Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "Good - that failed as expected");
        }

    }

    /* No longer testing in 7.4
    Now that property types are a thing, we need to pick a specific type, and stick with it.
    We're picking 'string'
    Something quicker that string would be nice. oh well.
    More importantly, I want quicker to define & lighter weight enums.
    */
    function noTestIn_7_4_plus_testDtoEnumSmallPrimes() {
        $obj = new CfdEnumSmallPrimes(['EnumValue' => 1]);
        $this->assertTrue($obj->EnumValue == 1, "Good");


        try {
            $obj = new \testworld\CfdEnumSmallPrimes(['EnumValue' => 4]);
            $this->assertTrue(0, "Should not get this far");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "Good - that failed as expected");
        }



         try {
            $obj = new \testworld\CfdEnumSmallPrimes(['EnumValue' => 6]);
            $this->assertTrue(0, "Should not get this far cuz a string");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "Good - that failed as expected");
        }

         try {
            $obj = new \testworld\CfdEnumSmallPrimes(['EnumValue' => '3']);
            $this->assertTrue(0, "Should not get this far");
         } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "Good - that failed as expected");
        }

    }
}
