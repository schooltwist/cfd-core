<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

// --- Hello World -----------------
class IsEmpty extends \SchoolTwist\Cfd\Core\CfdBase
{
}

// --- Simple Access -----------------
class HasItemNoDefault extends \SchoolTwist\Cfd\Core\CfdBase
{
    public int $value; //all public properties MUST be typed.
}


class HasItemMaybeNullNoDefault extends \SchoolTwist\Cfd\Core\CfdBase
{
    public ?int $value; // Null is ok
}

class HasItemMaybeNullDefaultsToNull extends \SchoolTwist\Cfd\Core\CfdBase
{
    public ?int $value = null;
}

class HasItemMaybeNullDefaultsToZero extends \SchoolTwist\Cfd\Core\CfdBase
{
    public ?int $value = 0;
}
class HasItemDefaultsToZero extends \SchoolTwist\Cfd\Core\CfdBase
{
    public int $value = 0;
}

// --- CompoundAccess -----------------
class HasCompoundAccess extends \SchoolTwist\Cfd\Core\CfdBase
{
    public int $value;
    public string $label;
}

// --- CustomValidation -----------------
class CustomValidationAsSmallPositive extends HasCompoundAccess {
    protected static function value_validates($dangerousValue) : \SchoolTwist\Validations\Returns\DtoValid {
        if ($dangerousValue >= 1 && $dangerousValue <=10) {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid'=>true]);
        } else {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid'=>false]);
            }
    }
}

// --- CustomValidation -----------------
class AlwaysCastsStringToInt extends \SchoolTwist\Cfd\Core\CfdBase {
    public int $pk;

}



/**
 * Class CfdBase
 * @package SchoolTwist\Cfd\Core
 *
 *  // Since extending CfdBase, public properties all validated at creation
 *  // Any public property w/o a default is required to be set at creation
 *  // Public properties can be read, but not updated. To update, create a new copy with different inputs
 */
class NamedThingsCfd extends \SchoolTwist\Cfd\Core\CfdBase
{
    # protected const  META_PREFIXES = ['doesHave'];  //
    #  public CONST ALLOW_UPCONVERT_TYPES = ['integer','string'];
    public string $lastName; // simple var, but it had better be a string when assigning

    public int $numEyes; // simple var, but it had better be a int when assigning
    public ?int $age; // simple var, but it had better be a int when assigning. It can be null if we don't know the age
    // It must be assigned something

    public string $namesIKnow;  // There is a function

    // Optional Custom Validation
    public static function namesIKnow_Validates($untrustedValue): \SchoolTwist\Validations\Returns\DtoValid
    {
        if (in_array($untrustedValue, ['Tom', 'Bill', 'Chad', 'Lisa', 'Wendy'])) {
            return new DtoValid(
                [
                    'isValid' => true,
                    'enumReason' => 'Found',
                    'message' => "I know a person named $untrustedValue."
                ]
            );
        } else {
            return new DtoValid(
                [
                    'isValid' => false,
                    'enumReason' => 'NotFound',
                    'message' => "name($untrustedValue) is not a name of somebody I went to school with."
                ]
            );
        }
    }
}
class C2 {
    public function __construct(array $a) {
        1/0;
    }
}

class TestDtoCfd_BaseString4 extends TestCase
{
    function test_HelloWorld() {
        //$a = new IsEmpty(); // Dies
        $a = new IsEmpty([]); // Can't do much
        $this->assertIsObject($a);
    }


    function test_ShowBasicInitializationAndAccess()
    {

        $a = new HasItemDefaultsToZero([]);
        $this->assertEquals(0,$a->value);

        $a = new HasItemDefaultsToZero(['value'=>1]);
        $this->assertEquals(1,$a->value);


        $a = new HasItemMaybeNullDefaultsToNull([]);
        $this->assertNull($a->value);

        $a = new HasItemMaybeNullDefaultsToNull(['value'=>1]);
        $this->assertEquals(1,$a->value);

        $a = new HasItemMaybeNullDefaultsToNull(['value'=>null]);
        $this->assertNull($a->value);

        $a = new HasItemMaybeNullDefaultsToZero([]);
        $this->assertEquals(0,$a->value);

        # $a = new HasItemMaybeNullNoDefault([]); // illegal cuz no default
        $a = new HasItemMaybeNullNoDefault(['value'=>2]);
        $this->assertEquals(2,$a->value);

        $a = new HasItemMaybeNullNoDefault(['value'=>null]);
        $this->assertNull($a->value);

        #$a = new HasItemNoDefault([]); // Fail cuz no default, so need something
        #$a = new HasItemNoDefault(['value'=>null]); // Fail cuz null not allowed
        $a = new HasItemNoDefault(['value'=>2]);
        $this->assertEquals(2,$a->value);

    }


    function testHasCompoundAccess() {
        $a = new HasCompoundAccess(['value'=>1, 'label'=>'Value']);
        $this->assertEquals(1,$a->value);
        $this->assertEquals('Value',$a->label);
    }

    function testValidation() {
        $dtoValid = CustomValidationAsSmallPositive::preValidateSubmission(['value'=>'Three', 'label'=>'Value']);
        $this->assertFalse($dtoValid->isValid); // wrong type
        $dtoValid = CustomValidationAsSmallPositive::preValidateSubmission(['value'=>'3', 'label'=>'Value']);
        $this->assertTrue($dtoValid->isValid); // wrong type, buuut, since '3' trivially converts to int(3), then we'll accept it
        $dtoValid = CustomValidationAsSmallPositive::preValidateSubmission(['value'=>'99', 'label'=>'Value']);
        $this->assertFalse($dtoValid->isValid); // validates_value sez out of range
        $dtoValid = CustomValidationAsSmallPositive::preValidateSubmission(['value'=>99, 'label'=>'Value']);
        $this->assertFalse($dtoValid->isValid); // fails cuz our custom 'validates_value' says so out of range

        $dtoValid = CustomValidationAsSmallPositive::preValidateSubmission(['value'=>3, 'label'=>'Value']);
        $this->assertTrue($dtoValid->isValid);


        $a = new CustomValidationAsSmallPositive(['value'=>3, 'label'=>'Value']);
        $this->assertEquals(3,$a->value);

        $dtoValid = CustomValidationAsSmallPositive::preValidateSubmission(['value'=>-1, 'label'=>'Value']);
        $this->assertFalse($dtoValid->isValid);

        try {
            $a = new CustomValidationAsSmallPositive(['value'=>-1, 'label'=>'Value']); // fails cuz 'validates_value' says so
            $this->assertTrue(false, "Should not get here: Not valid");
        } catch (\SchoolTwist\Cfd\Core\ErrorFromCfd $e) {
            $this->assertTrue(true, "This should have failed hard (and gotten here) cuz int != integer, and php numbers are integers.  int is slang for integer.: " . get_called_class() . "  " . __LINE__);
        }
    }
    function testCastsStringToInt() {
        $a = new AlwaysCastsStringToInt(['pk'=>1]);
        $this->assertEquals(1,$a->pk);

        $a = new AlwaysCastsStringToInt(['pk'=>'1']); // This works, and we use this all the time when reading from db.
        $this->assertEquals(1,$a->pk);

        #$c = new Simple_0_Cfd(['caresGiven' => 1.1]); // Dies: cuz 1.1 is a float, not an int
    }


}
